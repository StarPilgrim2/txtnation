#ifndef SKYPEACCOUNTWIDGETS_H
#define SKYPEACCOUNTWIDGETS_H

#include <QDialog>
#include "skypeaccountsmodel.h"

namespace Ui {
class SkypeAccountWidgets;
}

class SkypeAccountWidgets : public QDialog
{
    Q_OBJECT

public:
    explicit SkypeAccountWidgets(bool first,QWidget *parent = 0);
    ~SkypeAccountWidgets();

    QStringList interestedNames() const;

    QStringList interestedPathes() const;

    void updateModel();

public slots:
    int exec();
signals:
    void updateInterestedNames();
protected:
    void done(int code);
private:
    Ui::SkypeAccountWidgets *ui;
    SkypeAccountsModel m_model;
    bool m_first;

};

#endif // SKYPEACCOUNTWIDGETS_H

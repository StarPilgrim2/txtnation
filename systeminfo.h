#ifndef OSYSTEMINFO_H
#define OSYSTEMINFO_H

#include <QSharedDataPointer>
#include <QRect>

class SystemInfoData;
class QNetworkConfiguration;

class SystemInfo
{
public:


    enum WinVers
    {
        Unknown = 0,
        Win_7   = 1
    };

    static const SystemInfo& instance();
    ~SystemInfo();

    //������ OS, �� ������� ������ � ���� ��� �������� � ��������� �������
    const QString& OsVersion() const;
    //������ ���������� ���������
    const QList<QRect>& resolutions() const;
    //����� ���������� ������������� ��� ��������
    int primaryScreen() const;
    //�������� ���������� � ��� ���� ��� � ��������� �������
    const QString& processorBrand() const;
    //���������� ����������� ��� � ���������� �������� (��������� ��� ��������)
    const QString& memoryInfo() const;
    //������ ������� ���������
    const QStringList& networkInterfaces() const;
    //�������� ���������
    const QString& activeInterface() const;

    WinVers winVersion() const;
    //������� �����
    QString cpuFrequencyMhz() const;

    //��������� ���������� hex, ���, ��� ���� ������� ������ � ������
    QString motherBoardManufacturerId() const;

    qulonglong progUpTime() const;

    qulonglong allMemoryBytes() const;

    qulonglong availMemoryBytes() const;

    qulonglong selfApppMemoryBytes() const;

    qulonglong systemUpTimeSecs() const;

private:
    SystemInfo();

    QString getOsVersion(WinVers& vers);
    QString getProcessorBrand();
    QString getMemoryInfo();    
    void getInterfaces(QStringList& interfaces,QString& active);
    QString roamingStr(const QNetworkConfiguration& conf) const;
    QString networStateString(const QNetworkConfiguration& conf) const;
    QString getFrequencyCpuMhz() const;
    QString getMotherBoardManufacturerId() const;
    QSharedDataPointer<SystemInfoData> data;
};

#endif // OSYSTEMINFO_H

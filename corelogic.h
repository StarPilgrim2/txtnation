#ifndef CORELOGIC_H
#define CORELOGIC_H

#include <QObject>
#include <QScopedPointer>
#include <QList>
#include <QSqlDatabase>
#include <QThread>
#include "spreadsheetmanager.h"
#include "authgoogle.h"
#include "settings.h"
#include "networkmanager.h"



class SkypeWatcher;

class CoreLogic : public QObject
{
    Q_OBJECT
public:
    explicit CoreLogic(QObject *parent = 0);
    ~CoreLogic();
signals:
    void googleDriveAuthorized();
    void authDenied();
    void refreshGUI();
    void hideGUI();
    void authProblem();


public slots:
    void doAuthorize();
    void updateSettings();

    void finishWork();

    void openGoogleDrive();

private slots:
    void changeAccessToken(QString token);
    void refreshTokenReceive(QString token);
    void retriveData(bool allMessages=false);
    void authError();

    void contactOrGroupAdded(QString skype_id, QString googleId, bool isContact);
    void messagesAdded(QString skype_id,QDateTime uploadTo,QString lastregionA1);
    void folderWasCreated(QString name,QString id);
private:
   Settings m_settings; //for save and load internal data
   QScopedPointer<AuthGoogle> m_auth;// for Google authorization
   QScopedPointer<NetworkManager> m_networkApiManager; //for upload data to Google drive


   //QScopedPointer<SpreadsheetManager> m_spreadsheetManager;

   bool m_accessTokenReceived;

   //QStringList m_skypeAppUsers;
   QMap<QString,SkypeWatcher*> m_databaseConnections;
   QTimer m_uploadTimer;
   QTimer m_abortTimer;
   static const int m_abortInterval=7;
   bool m_alreadyFinished;
   static const QString m_driveUrl;

};

class CoreThread: public QThread
{
    Q_OBJECT
public:
    explicit CoreThread(CoreLogic* logic, QObject* parent=0);
    ~CoreThread();
private:
     CoreLogic* m_logic;
};

#endif // CORELOGIC_H

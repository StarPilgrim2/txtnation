#ifndef HTTPSERVER_H
#define HTTPSERVER_H

#include <QScopedPointer>
#include <QTcpServer>
#include <QTcpSocket>
#include <QScopedPointer>



class AuthHttpServer : public QObject {
    Q_OBJECT
public:
    AuthHttpServer(QObject* parent = 0);
    virtual ~AuthHttpServer();

    QString loopbackUrl() const;

    bool listen();

signals:
    void authCode(QString code);
    void authDenied();

private slots:
    void newConnection();
    void readData();
    void closeConnection();
    void errorHandler(QAbstractSocket::SocketError err);


private:

    void processing(QByteArray& responseBuffer,QTcpSocket* socket);
    //bool getRequest(QTcpSocket *socket, int timeout, QString &authCode);
    QString prepareResponse(QString body, bool httpOk) const;

    QString prepareRedirectTo(QString location) const;

    QTcpServer m_server;
    //QScopedPointer<QTcpSocket,QScopedPointerObjectDeleteLater<QTcpSocket>> m_socket;

    //static const int m_timeout=100;
    QMap<QTcpSocket*,QByteArray> m_responseBuffer;

};


#endif // HTTPSERVER_H

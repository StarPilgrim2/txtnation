#include "spreadsheetmanager.h"
#include "common.h"
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrlQuery>
#include <QJsonDocument>
#include <QUuid>
#include <QEvent>

const QString SpreadsheetManager::m_driveAPIUrl="https://www.googleapis.com/drive/v3/files";
const QString SpreadsheetManager::m_sheetAPiUrl="https://sheets.googleapis.com/v4/spreadsheets/";
const QString SpreadsheetManager::m_mimespreadSheet="application/vnd.google-apps.spreadsheet";
const QString SpreadsheetManager::m_folderMime="application/vnd.google-apps.folder";

SpreadsheetManager::SpreadsheetManager(QNetworkAccessManager* network, QObject *parent) : QObject(parent),m_networkManager(network),m_secs(defaultNetworkInterval())
{

}

void SpreadsheetManager::setTimeout(int secs)
{
   if (!secs)
   {
       qWarning() << "NETWORK: try to set zero timeout!";
       return;
   }
   m_secs = secs;
}


void SpreadsheetManager::setAccessToken(QString token)
{
    m_accessToken=token;
}

void SpreadsheetManager::addSpreadSheet(QString name,QString skype_id,bool spreadSheetIscontact)
{
    if (m_googleFolderId.isEmpty())
    {
        qCritical() << "NETWORK: folder id is empty and can't crete file";
        return;
    }

    QNetworkRequest request;
    request.setUrl(m_driveAPIUrl);

    prepareAuthData(request);

    QVariantMap body;
    body["mimeType"] = m_mimespreadSheet;
    body["name"] =  name;
    body["parents"] = QVariantList() << m_googleFolderId;

    QByteArray data = QJsonDocument::fromVariant(body).toJson();

    qDebug() << data;

    QMap<QString,QVariant> metaParam;
    metaParam.insert("isContact",spreadSheetIscontact);

    return createRequest(SpreadsheetManager::CreateSpreadsheet,request,data,skype_id,metaParam);
}

void SpreadsheetManager::addValuesToSheet(QString sheetId, const QList<QStringList> &values,QString skype_id)
{
    if (values.isEmpty() || values.value(0).isEmpty())
        return;

    QString A1notation("A1%3A");

    int row = values.count();
    int col = values.value(0).count();

    O_ASSERT(col<=26);

    col %= 26;

    A1notation += QChar::fromLatin1(col+64) + QString::number(row);

    QUrl url(m_sheetAPiUrl + sheetId + "/values/" + A1notation+":append");

    QUrlQuery query;
    query.addQueryItem("insertDataOption","INSERT_ROWS");
    query.addQueryItem("valueInputOption","USER_ENTERED");

    url.setQuery(query);

    QNetworkRequest request;
    request.setUrl(url);

    qDebug() << request.url().toString();

    prepareAuthData(request);


    QVariantList temp;

    foreach (QList<QString> val, values)
    {
        temp.append(QVariant(val));
    }

    //QVariant tempVar(temp);

    QVariantMap tempMap;

    tempMap["majorDimension"] = "ROWS";
    tempMap["range"]  = A1notation.replace("%3A",":");
    tempMap["values"] = temp;

    QByteArray data = QJsonDocument::fromVariant(tempMap).toJson();

    //a lot of data!!!
    //qDebug() << data;

    return createRequest(SpreadsheetManager::AppendValues,request,data,skype_id);

}

void SpreadsheetManager::getSpreadSheet(QString name, QString skype_id, bool spreadSheetIscontact)
{
    QNetworkRequest request;

    QUrl url(m_driveAPIUrl);
    QUrlQuery query;

    query.addQueryItem("q","trashed = false and name = '"+ name + "' and mimeType = '" + m_mimespreadSheet + "'");
    url.setQuery(query);

    qDebug() << url;

    request.setUrl(url);

    prepareAuthData(request);


    QMap<QString,QVariant> metaParam;
    metaParam.insert("isContact",spreadSheetIscontact);

    return createRequest(SpreadsheetManager::GetSpreadsheet,request,QByteArray(),skype_id,metaParam,false);
}

void SpreadsheetManager::getFolderOnDrive(QString name,QString parentId)
{
    QNetworkRequest request;

    QUrl url(m_driveAPIUrl);
    QUrlQuery query;

    QString q = "trashed = false and  name = '"+ name + "' and mimeType = '" + m_folderMime + "'";

    if (!parentId.isEmpty())
        q += " and '" + parentId + "' in parents";

    query.addQueryItem("q",q);
    url.setQuery(query);

    qDebug() << url;

    request.setUrl(url);

    prepareAuthData(request);


    QMap<QString,QVariant> metaParam;
    metaParam.insert("folderName",name);

    return createRequest(SpreadsheetManager::GetFolder,request,QByteArray(),QString(),metaParam,false);
}

void SpreadsheetManager::createFolderOnDrive(QString name,QString parentId)
{
    QNetworkRequest request;
    request.setUrl(m_driveAPIUrl);

    prepareAuthData(request);

    QVariantMap body;
    body["mimeType"] = m_folderMime;
    body["name"] =  name;

    if (!parentId.isEmpty())
        body["parents"] = QStringList() << parentId;

    QByteArray data = QJsonDocument::fromVariant(body).toJson();

    qDebug() << data;

    QMap<QString,QVariant> metaParam;
    metaParam.insert("folderName",name);

    return createRequest(SpreadsheetManager::CreateFolder,request,data,QString(),metaParam);
}

bool SpreadsheetManager::accessTokenExist() const
{
    return !m_accessToken.isEmpty();
}

void SpreadsheetManager::setGoogleFolderId(QString id)
{
    m_googleFolderId = id;
}

QString SpreadsheetManager::googleFolderId() const
{
    return m_googleFolderId;
}

void SpreadsheetManager::setGoogleAdditonalFolderId(QString id)
{
    m_googleAdditionalFolderId = id;
}

void SpreadsheetManager::replyFinished()
{
    QNetworkReply* reply = dynamic_cast<QNetworkReply*>(sender());

    if (!reply)
    {
        qCritical() << "NETWORK: reply is null!!!";
        return;
    }

    QByteArray data       = reply->readAll();
    QString skype_id      = reply->property("uid").toString();
    int type              = reply->property("type").toInt();
    bool isContact        = reply->property("isContact").toBool();
    QString folderName    = reply->property("folderName").toString();
    QNetworkReply::NetworkError replyError        = reply->error();
    //QString replyErrorStr = reply->errorString();

    qInfo() << data;

    replyAbort(reply);

    if (replyError != QNetworkReply::NoError)
    {
        qCritical() << "NETWORK: error in unexpected place!";

        errorHandler(replyError,reply);
        return;
    }

    QJsonParseError jsonError;
    QVariant jsonData = QJsonDocument::fromJson(data,&jsonError).toVariant();

    if (!jsonData.isValid() || jsonError.error != QJsonParseError::NoError)
    {
        emit errorGoogleApi(JSON_ERROR,(RequestType)type,QMap<QString,QString>(),jsonError.errorString());
        return;
    }


    QString appError;
    bool ok=false;

    if (type == SpreadsheetManager::CreateSpreadsheet)
    {

        ok = parseCreateSpreadsheet(skype_id,jsonData,appError,isContact,true);
    }
    if (type == SpreadsheetManager::AppendValues)
    {
        ok = parseAddValuesToSheet(skype_id,jsonData,appError);
    }
    if (type == SpreadsheetManager::GetSpreadsheet)
    {
        ok =  parseCreateSpreadsheet(skype_id,jsonData,appError,isContact,false);
    }
    if (type == SpreadsheetManager::GetFolder)
    {
        ok = psrseGetFolder(jsonData,appError,folderName);
    }
    if (type == SpreadsheetManager::CreateFolder)
    {
        ok = psrseCreateFolder(jsonData,appError,folderName);
    }

    if (!ok)
    {
        emit errorGoogleApi(JSON_ERROR,(RequestType)type,QMap<QString,QString>(),appError);
        qCritical() << "NETWORK: error in unexpected place!";
    }

//    if (!ok)
//        emit errorGoogleApi(skype_id,appError,(SpreadsheetManager::RequestType)type);

}

void SpreadsheetManager::errorHandler(QNetworkReply::NetworkError err,QNetworkReply* reply)
{
    if (!reply)
        reply = dynamic_cast<QNetworkReply*>(sender());

    if (!reply)
        return;

    replyAbort(reply);

    RequestType reqType      = (RequestType)reply->property("type").toInt();
    ErrorType   errType      = OTHER_NETWORK_ERROR;
    QString     errtext;

    QMap<QString,QString> metaParam;

    for (QByteArray arr:reply->dynamicPropertyNames())
        metaParam[arr] = reply->property(arr).toString();


    QVariant httpCodeData = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);

    if (!httpCodeData.isValid())
    {
        errtext = reply->errorString() + "Qt networc code" + QString::number(err);
        emit errorGoogleApi(errType,reqType,metaParam,errtext);
        return;
    }

    quint16 httpCode = httpCodeData.toUInt();

    QJsonParseError jsonError;
    QByteArray data       = reply->readAll();
    QVariant jsonData = QJsonDocument::fromJson(data,&jsonError).toVariant();

    QString jsonMessage;
    QString jsonReason;

    if (!jsonData.isValid() || jsonError.error != QJsonParseError::NoError)
       qDebug() << "NETWORK: error json couldn't parse";
    else
        parseJsonError(data,jsonReason,jsonMessage);

    if (jsonMessage.isEmpty())
        errtext = data;
    else
        errtext = jsonMessage;

    if (httpCode == 400)
        errType = BAD_REQUEST;
    else if (httpCode == 401)
        errType = INVALID_CREDENTIALS;
    else if (httpCode == 403)
    {
        if (jsonReason == "dailyLimitExceeded")
            errType = DAILY_LIMIT;
        else if (jsonReason == "userRateLimitExceeded")
            errType = USERRATE_LIMIT;
        else if (jsonReason == "rateLimitExceeded")
            errType = RATE_LIMIT;
        else
            errType = USER_PERM_PROBLEMS;
    }
    else if (httpCode == 404 )
        errType = FILE_NOT_FOUND;
    else if (httpCode == 500)
        errType = BACKEND_ERR;
    else
        errType = OTHER_HTTP_ERROR;

    emit errorGoogleApi(errType,reqType,metaParam,errtext);

}

void SpreadsheetManager::parseJsonError(QByteArray data, QString &reason, QString &message)
{
    QJsonParseError jsonError;
    QVariant jsonData = QJsonDocument::fromJson(data,&jsonError).toVariant();

    if (!jsonData.isValid() || jsonError.error != QJsonParseError::NoError)
    {
        qDebug() << "couldn't parse error json";
        return;
    }

    QVariantMap vmap = jsonData.toMap();

    QVariantMap verr = vmap.value("error").toMap();

    message = verr.value("message").toString();

    reason = verr.value("errors").toList().value(0).toMap().value("reason").toString();

}

void SpreadsheetManager::prepareAuthData(QNetworkRequest &request)
{
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader(QByteArray("Authorization"),QString("Bearer " + m_accessToken).toLatin1());
}

void SpreadsheetManager::createRequest(SpreadsheetManager::RequestType type, QNetworkRequest &request, const QByteArray &data,QString skype_id,const QMap<QString,QVariant>& metaParam,bool isPost)
{
    QNetworkReply* reply = 0;

    if (isPost)
        reply =  m_networkManager->post(request,data);
    else
        reply =  m_networkManager->get(request);

    if (!reply)
    {
        qCritical() << "NETWORK: reply is empty!";
        return ;
    }

    connect(reply,SIGNAL(finished()),this,SLOT(replyFinished()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),this,SLOT(errorHandler(QNetworkReply::NetworkError)));

    //QString uid = QUuid::createUuid().toString();

    reply->setProperty("uid",skype_id);
    reply->setProperty("type",type);

    if (m_secs)
    {
        reply->startTimer(1000*m_secs);
        reply->installEventFilter(this);
    }
    else
    {
        qCritical() << "NETWORK: zero timeout!!!";
    }


    QMap<QString, QVariant>::const_iterator it = metaParam.begin();

    while (it != metaParam.end())
    {
        reply->setProperty(it.key().toLatin1(),it.value());
        it++;
    }
}

void SpreadsheetManager::replyAbort(QNetworkReply *reply)
{
    if (!reply)
    {
        qCritical() << "NETWORK: try to abort zero reply!";
        return;
    }

    reply->disconnect();
    reply->deleteLater();
}

bool SpreadsheetManager::eventFilter(QObject * obj, QEvent * event)
{
    QNetworkReply* reply = dynamic_cast<QNetworkReply*>(obj);

    if (event->type() == QEvent::Timer && reply)
    {
        replyAbort(reply);

        RequestType reqType      = (RequestType)reply->property("type").toInt();

        QMap<QString,QString> metaParam;

        for (QByteArray arr:reply->dynamicPropertyNames())
            metaParam[arr] = reply->property(arr).toString();

        emit errorGoogleApi(SELF_TIMEOUT_REACH,reqType,metaParam,"timeout exceed");

    }

    return QObject::eventFilter(obj,event);

}


bool SpreadsheetManager::parseCreateSpreadsheet(QString skype_id, const QVariant &jsonData, QString &appError,bool isContact,bool isCreate)
{
    if (jsonData.isNull() || !jsonData.canConvert(QMetaType::QVariantMap))
    {
        appError = "not valid json";
        return false;
    }

    QString fileId;

    if (isCreate)
        fileId = jsonData.toMap().value("id").toString();
    else
        fileId = jsonData.toMap().value("files").toList().value(0).toMap().value("id").toString();

    if (fileId.isEmpty())
    {
        appError = QJsonDocument::fromVariant(jsonData).toJson();
        return false;
    }

    emit fileCreated(skype_id,fileId,isContact);

    return true;
}

bool SpreadsheetManager::parseAddValuesToSheet(QString skype_id, const QVariant &jsonData, QString &appError)
{
    if (jsonData.isNull() || !jsonData.canConvert(QMetaType::QVariantMap))
    {
        appError = "not valid json";
        return false;
    }

    QString tableRange = jsonData.toMap().value("updates").toMap().value("updatedRange").toString();

    if (tableRange.isEmpty())
    {
        appError = QJsonDocument::fromVariant(jsonData).toJson();
        return false;
    }

    emit valuesAdded(skype_id,tableRange);

    return true;
}

bool SpreadsheetManager::psrseGetFolder(const QVariant &jsonData, QString &appError, QString folderName)
{
    if (jsonData.isNull() || !jsonData.canConvert(QMetaType::QVariantMap))
    {
        appError = "not valid json";
        return false;
    }

    QString fileId = jsonData.toMap().value("files").toList().value(0).toMap().value("id").toString();

    if (fileId.isEmpty())
        qWarning() << jsonData;

    emit folderIsExist(folderName,fileId,!fileId.isEmpty());

    return true;
}

bool SpreadsheetManager::psrseCreateFolder(const QVariant &jsonData, QString &appError, QString folderName)
{
    if (jsonData.isNull() || !jsonData.canConvert(QMetaType::QVariantMap))
    {
        appError = "not valid json";
        return false;
    }

    QString fileId = jsonData.toMap().value("id").toString();

    if (fileId.isEmpty())
    {
        appError = QJsonDocument::fromVariant(jsonData).toJson();
        return false;
    }

    emit folderCreated(folderName,fileId);

    return true;
}

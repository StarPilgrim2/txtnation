#ifndef SPREADSHEETQUEUE_H
#define SPREADSHEETQUEUE_H

#include <QObject>
#include <QSharedPointer>
#include "spreadsheetmanager.h"

class SpreadsheetQueue : public QObject
{
    Q_OBJECT
public:
    explicit SpreadsheetQueue(QObject *parent, SpreadsheetManager *spreadsheet, QString skypeId, QString accessToken, const QString& googleSheetID = QString());

    void addSpreadSheet(QString name, bool isContact);

    void addMessages(QList<QStringList> data,QDateTime when);

    void setAccessToken(QString token);

    void setSpreadSheetId(QString sheetId);

    void processQueue();

    bool isActiveRequest() const;

signals:
    void contactOrGroupAdded(QString skype_id,QString googleId,bool isContact);

    void messagesAdded(QString skype_id,QDateTime uploadTo,QString lastregionA1);

    void errorGoogleApi(SpreadsheetManager::ErrorType errType,SpreadsheetManager::RequestType reqType,QMap<QString,QString> metaParam,QString errorText);

private slots:
    void error(SpreadsheetManager::ErrorType errType, SpreadsheetManager::RequestType reqType, QMap<QString, QString> metaParam, QString errorText);

    void fileCreated(QString skype_id, QString googlefileId, bool isContact);
    void valuesAdded(QString skype_id,QString actualRange);

private:


    QString m_skypeId;
    bool m_needAddSpreadsheet;
    QString m_spreadSheetName;
    QList<QPair<QList<QStringList>,QDateTime>> m_queue;
    SpreadsheetManager* m_spreadsheet;
    bool m_activeRequest;
    QString m_googleSheetId;
    bool m_spreadSheetIscontact;
    bool m_needCheckFileCreate;
};

#endif // SPREADSHEETQUEUE_H

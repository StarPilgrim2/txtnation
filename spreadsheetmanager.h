#ifndef SPREADSHEETMANAGER_H
#define SPREADSHEETMANAGER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QSharedDataPointer>
#include <QNetworkReply>

class SpreadsheetManager : public QObject
{
    Q_OBJECT
public:

    enum RequestType
    {
        CreateSpreadsheet=0,
        AppendValues,
        GetSpreadsheet,
        GetFolder,
        CreateFolder
    };

    enum ErrorType
    {
        BAD_REQUEST=0,
        INVALID_CREDENTIALS,
        DAILY_LIMIT,
        USERRATE_LIMIT,
        RATE_LIMIT,
        USER_PERM_PROBLEMS,
        FILE_NOT_FOUND,
        BACKEND_ERR,
        OTHER_HTTP_ERROR,
        OTHER_NETWORK_ERROR,
        SELF_TIMEOUT_REACH,
        JSON_ERROR

    };


    explicit SpreadsheetManager(QNetworkAccessManager *network, QObject *parent = 0);

    void setTimeout(int secs);

    void setAccessToken(QString token);

    //skype_id will emit out
    void addSpreadSheet(QString name, QString skype_id, bool spreadSheetIscontact);

    void addValuesToSheet(QString sheetId, const QList<QStringList> &values, QString skype_id);

    void getSpreadSheet(QString name, QString skype_id, bool spreadSheetIscontact);

    void getFolderOnDrive(QString name, QString parentId=QString());

    void createFolderOnDrive(QString name, QString parentId=QString());

    bool accessTokenExist() const;

    void setGoogleFolderId(QString id);

    QString googleFolderId() const;

    void setGoogleAdditonalFolderId(QString id);

    bool eventFilter(QObject * obj, QEvent * event);

signals:
    void errorGoogleApi(SpreadsheetManager::ErrorType errType,SpreadsheetManager::RequestType reqType,QMap<QString,QString> metaParam,QString errorText);

    void fileCreated(QString skype_id,QString googlefileId,bool isContact);
    void valuesAdded(QString skype_id,QString actualRange);

    void folderIsExist(QString name,QString googlefileId,bool isExis);
    void folderCreated(QString name,QString googlefileId);

private slots:
    void replyFinished();
    void errorHandler(QNetworkReply::NetworkError err, QNetworkReply *reply=0);

private:

    void parseJsonError(QByteArray data, QString& reason,QString& message);

    void prepareAuthData(QNetworkRequest& request);

    void createRequest(RequestType type, QNetworkRequest& request, const QByteArray& data, QString skype_id,const QMap<QString, QVariant>& metaParam=QMap<QString, QVariant>(),bool isPost=true);

    void replyAbort(QNetworkReply *reply);

    bool parseCreateSpreadsheet(QString skype_id, const QVariant& jsonData, QString& appError, bool isContact,bool isCreate);
    bool parseAddValuesToSheet(QString skype_id,const QVariant& jsonData,QString& appError);
    bool psrseGetFolder(const QVariant& jsonData,QString& appError,QString folderName);
    bool psrseCreateFolder(const QVariant& jsonData,QString& appError,QString folderName);

    QNetworkAccessManager* m_networkManager;
    QString m_accessToken;
    QString m_googleFolderId;
    QString m_googleAdditionalFolderId;
    int m_secs;

    static const QString m_driveAPIUrl;
    static const QString m_mimespreadSheet;
    static const QString m_sheetAPiUrl;
    static const QString m_folderMime;
    //static const int m_defaultTimeOut=30;

    //static const QString m_sheetFolder;
};

#endif // SPREADSHEETMANAGER_H

#ifndef SKYPEWATCHER_H
#define SKYPEWATCHER_H

#include <QObject>
#include <QFileSystemWatcher>
#include <QSet>
#include <QMap>
#include <QTimer>
#include "skypereader.h"

class SkypeWatcher: public QObject
{
    Q_OBJECT
public:
    SkypeWatcher(QString file, QStringList contacts,QStringList groupChats, QMap<QString, QDateTime> lastmessageDate, QObject *parent=0);

    QMap<QString,QString>  newNormalContactList();

    //only disp name,only for active chats
    QMap<QString,QString> newGroupchatList();

    //key is skype_id or group chat name, list contains 3 element - author,date,message
    //without agument get all messages
    bool newMessages(QMap<QString, QList<QStringList>>& messages, QMap<QString, ListEditedMessage> &editedMessages, QMap<QString, ListDeletedMessage> &deletedMessages, QMap<QString, QDateTime> &lastmessageDate, QMap<QString, QDateTime> &lastEditedMessageDate, QMap<QString, QDateTime> &lastDeltedMessageDate);

    //force all message, without buffer
    bool allMessages(QMap<QString, QList<QStringList>>& messages,QMap<QString,ListEditedMessage>& editedMessages,QMap<QString,ListDeletedMessage>& deletedMessages,
                     QMap<QString, QDateTime> &lastmessageDate, QMap<QString, QDateTime> &lastEditedMessageDate,QMap<QString, QDateTime> &lastDeltedMessageDate);

    //timer starts
    void startPeridicallyread();

    //this calling will be say 'data have been sent logic. don't give me those data anymore'
    //void acceptData();

private slots:
    void mainDbUpdated();

private:
    bool retrieveMessages(QMap<QString,QList<QStringList>>& messages, QMap<QString, ListEditedMessage> &editedMessages, QMap<QString, ListDeletedMessage> &deletedMessages,
                          QMap<QString, QDateTime> &lastmessageDate,QMap<QString, QDateTime> &lastEditedMessageDate,QMap<QString, QDateTime> &lastDeltedMessageDate);

    QDateTime lastDate(QList<QStringList> messages) const;
    QDateTime lastDate(ListEditedMessage messages) const;
    QDateTime lastDate(ListDeletedMessage messages) const;

    //QFileSystemWatcher m_watcher;
    SkypeReader m_reader;

    QSet<QString> m_users;
    QSet<QString> m_chatGroups;
    QMap<QString, QDateTime> m_lastmessageDate;
    QMap<QString, QDateTime> m_lastEditedMessageDate;
    QMap<QString, QDateTime> m_lastDeletedmessageDate;
    QMap<QString,QList<QStringList>> m_messages;
    QMap<QString,ListEditedMessage> m_EditedMessages;
    QMap<QString,ListDeletedMessage> m_DeletetedMessages;
    QTimer m_timer;
    static const int m_readtimeout=1000; //ms

};

#endif // SKYPEWATCHER_H

#include "skypeaccountwidgets.h"
#include "ui_skypeaccountwidgets.h"

SkypeAccountWidgets::SkypeAccountWidgets(bool first, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SkypeAccountWidgets),m_model(first),m_first(first)
{
    ui->setupUi(this);
    ui->tableView->setModel(&m_model);

    ui->tableView->resizeColumnsToContents();
}

SkypeAccountWidgets::~SkypeAccountWidgets()
{
    delete ui;
}

QStringList SkypeAccountWidgets::interestedNames() const
{
    return m_model.interestednames();
}

QStringList SkypeAccountWidgets::interestedPathes() const
{
    return m_model.interestedPaths();
}

void SkypeAccountWidgets::updateModel()
{
    m_model.reloadData();
}

void SkypeAccountWidgets::done(int code)
{
    if (code == QDialog::Accepted)
    {
        if (m_first)
            m_model.save();
        else
            emit updateInterestedNames();
    }
    QDialog::done(code);
}

int SkypeAccountWidgets::exec()
{
    //m_model.reloadData();
    return QDialog::exec();
}

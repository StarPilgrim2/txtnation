#ifndef SKYPEREADER_H
#define SKYPEREADER_H

#include <QObject>
#include <QSqlDatabase>
#include <QDateTime>
#include "common.h"



class SkypeReader : public QObject
{
    Q_OBJECT
public:
    explicit SkypeReader(QString path,QObject *parent = 0);

    ~SkypeReader();

    //first is skype name and second is full name
    QPair<QString,QString> account() const;


    //list of pair, key is skype_id, valued is dispname for contact and dispname
    //only authorized users
    QMap<QString,QString>  normalContactList() const;

    //only disp name,only for active chats
    QMap<QString,QString> groupchatList() const;

    //key is skype_id or group chat name, list contains 3 element - author,date,message
    //without agument get all messages
    //return true if transaction is sucessfylly begin
    bool newMessages(const QMap<QString,QDateTime> &afterThan,QMap<QString,QList<QStringList>>& res/*,QMap<QString,bool>& updated*/);

    bool newEditedMessages(const QMap<QString,QDateTime> &afterThan,QMap<QString,ListEditedMessage>& res);

    bool newDeletedMessages(const QMap<QString,QDateTime> &afterThan,QMap<QString,ListDeletedMessage>& res);

private:
    void applyPragma(QStringList pragmas);

    bool newMessages(QString convo_id, const QDateTime& afterThan, QList<QStringList>& res);

    bool newEditedMessages(QString convo_id, const QDateTime& afterThan, ListEditedMessage& res);

    bool newDeltedMessages(QString convo_id, const QDateTime& afterThan, ListDeletedMessage& res);

    QSqlDatabase m_connection;
};




#endif // SKYPEREADER_H

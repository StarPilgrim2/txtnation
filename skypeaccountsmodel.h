#ifndef SKYPEACCOUNTSMODEL_H
#define SKYPEACCOUNTSMODEL_H

#include <QAbstractTableModel>
#include "settings.h"
#include "skypereader.h"

class SkypeAccountsModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    SkypeAccountsModel(bool first,QObject* parent=0);

    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData ( const QModelIndex & index, const QVariant & value, int role = Qt::EditRole );
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    QStringList interestedPaths() const;
    QStringList interestednames() const;

    void reloadData();

public slots:
    void save();

private:
    QStringList m_acounts;
    QStringList m_displays;
    QStringList m_paths;
    QList<int> m_checks;
    int m_defaultChecedIndex;
    bool m_first;
    Settings m_settings;

    static const int m_checkboxIndex=2;
    static const int m_columnCount=3;
};

#endif // SKYPEACCOUNTSMODEL_H

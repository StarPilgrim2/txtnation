#include <algorithm>
#include "spreadsheetqueue.h"
#include "networkmanager.h"
#include "common.h"

const QString NetworkManager::m_googleDriveFolder("Skype Backup");
const QString NetworkManager::m_googleAdditinalDriveFolder("Skype Changes");

NetworkManager::NetworkManager(QString googleFolderId, QObject *parent) : QObject(parent),m_network(new QNetworkAccessManager(this)),m_spreadsheetAPI(new SpreadsheetManager(m_network,this)),m_procesTimer(this),m_backofStepNumber(1)
{
    //m_procesTimer.setInterval(m_processEveryMs + (qrand() % m_widthRandom));
    connect(&m_procesTimer,SIGNAL(timeout()),this,SLOT(processNetwork()));

    connect(m_spreadsheetAPI,SIGNAL(folderIsExist(QString,QString,bool)),this,SLOT(folderIsExist(QString,QString,bool)));
    connect(m_spreadsheetAPI,SIGNAL(folderCreated(QString,QString)),this,SLOT(folderCreated(QString,QString)));

    m_spreadsheetAPI->setGoogleFolderId(googleFolderId);
}

void NetworkManager::addData(QMap<QString, QString> constacts, QMap<QString, QString> groupsChats, QMap<QString, QList<QStringList> > messages, QMap<QString,QDateTime> updatedTo, QMap<QString, QString> googleSheetsIds)
{
    qInfo() << "NETWORK: append new data";

    qInfo() << "NETWORK: has message for number of accounts = " << messages.count();

    bool queueInserted = false;

    QMap<QString, QList<QStringList>>::iterator itMessage =  messages.begin();

    while (itMessage != messages.end())
    {
        QString skype_id = itMessage.key();

        QList<QStringList> messages = itMessage.value();

        if (messages.isEmpty())
        {
            itMessage++;
            continue;
        }

        bool isConstact = constacts.contains(skype_id);
        bool isGroup    = groupsChats.contains(skype_id);


        if ((!isConstact && !isGroup) || (isConstact && isGroup))
        {
            qCritical() << "NETWORK: try to add data to not cantact and non group";

            itMessage++;
            continue;

        }


        SpreadsheetQueue* queue = m_queues.value(skype_id);

        if (!queue)
        {
            qInfo() << "NETWORK: create queue for " << skype_id;

            queueInserted = true;
            QString googleSheetId = googleSheetsIds.value(skype_id);

            queue = createQueue(skype_id,googleSheetId);

            if (googleSheetId.isEmpty())
            {
                QString spreadSheetname;

                if (isConstact)
                {
                    spreadSheetname = skype_id;

                    QString dispName = constacts.value(skype_id);

                    if (!dispName.isEmpty())
                        spreadSheetname = dispName + "(" + spreadSheetname + ")";
                }
                else
                {
                    spreadSheetname = groupsChats.value(skype_id);
                }

                qInfo() << "NETWORK: need to create new spreadsheet " << spreadSheetname;

                queue->addSpreadSheet(spreadSheetname,isConstact);
            }

        }

        queue->addMessages(messages,updatedTo.value(skype_id));


        itMessage++;

    }


    if (queueInserted)
    {
        qInfo() << "queue iterator was invalidated";
        m_iterator = m_queues.begin();
    }

}

void NetworkManager::startProcessing()
{
    //qInfo() << "NETWORK: start timer, duration =" << m_procesTimer.interval();

    timerRestartInital();

    m_iterator = m_queues.begin();
}

bool NetworkManager::endProcessing()
{
    qInfo() << "NETWORK: stop timer";
    m_procesTimer.stop();
    m_backofStepNumber=1;

    if (m_accessToken.isEmpty())
        return true;

    bool canFinish=true;

    QMap<QString,SpreadsheetQueue*>::iterator it  = m_queues.begin();

    while (it != m_queues.end())
    {
        SpreadsheetQueue* queue = it.value();

        if (!queue)
        {
            qCritical() << "queue is null!";
            it++;
            continue;
        }

        qInfo() << "queue for " << it.key() << "is active? = " << queue->isActiveRequest();

        canFinish &= queue->isActiveRequest();

        it++;

    }

    return !canFinish;
}

void NetworkManager::folderIsExist(QString name, QString id, bool exist)
{
    if (name == m_googleDriveFolder)
    {
        if (exist)
        {
            emit folderWasCreated(name,id);

            m_spreadsheetAPI->setGoogleFolderId(id);

            //startProcessing();

            m_spreadsheetAPI->getFolderOnDrive(m_googleAdditinalDriveFolder,id);
        }
        else
            m_spreadsheetAPI->createFolderOnDrive(m_googleDriveFolder);
    }
    if (name == m_googleAdditinalDriveFolder)
    {
        if (exist)
        {
            emit folderWasCreated(name,id);

            m_spreadsheetAPI->setGoogleAdditonalFolderId(id);

            startProcessing();
        }
        else
            m_spreadsheetAPI->createFolderOnDrive(m_googleAdditinalDriveFolder,m_spreadsheetAPI->googleFolderId());
    }

}

void NetworkManager::folderCreated(QString name, QString id)
{
    emit folderWasCreated(name,id);

    if (name == m_googleDriveFolder)
    {
        m_spreadsheetAPI->setGoogleFolderId(id);

        m_spreadsheetAPI->getFolderOnDrive(m_googleAdditinalDriveFolder,id);
    }
    if (name == m_googleAdditinalDriveFolder)
    {
        m_spreadsheetAPI->setGoogleAdditonalFolderId(id);
        startProcessing();
    }
}

void NetworkManager::setAcessToken(QString token)
{
    qInfo() << "NETWORK: token received";
    bool needStart = m_accessToken.isEmpty();

    m_accessToken = token;
    //std::for_each(m_queues.begin(),m_queues.end(),[=](SpreadsheetQueue* queue){if (queue) queue->setAccessToken(token);});
    m_spreadsheetAPI->setAccessToken(token);

    if (needStart)
        checkFolder();
    //startProcessing();

}

void NetworkManager::processNetwork()
{
    //qInfo() << "NETWORK: processing queue";


    if (m_queues.isEmpty())
        return;

    if (m_iterator == m_queues.end())
        m_iterator = m_queues.begin();

    m_iterator.value()->processQueue();

    m_iterator++;
}

void NetworkManager::errorHandler(SpreadsheetManager::ErrorType errType,SpreadsheetManager::RequestType reqType,QMap<QString,QString> metaParam,QString errorText)
{
    //    if (type == SpreadsheetManager::CreateFolder)
    //    {
    //        qCritical() << "CORE: can't create folder. Can't start upload!";
    //    }

    qDebug() << "NETWORK: google api error" << errType << reqType << metaParam << errorText;

    if (errType == SpreadsheetManager::RATE_LIMIT || errType == SpreadsheetManager::USERRATE_LIMIT ||
            errType == SpreadsheetManager::BACKEND_ERR || errType == SpreadsheetManager::OTHER_HTTP_ERROR ||
            errType == SpreadsheetManager::OTHER_NETWORK_ERROR)
    {
        timerRestartExponental();
        return;
    }

    if (errType == SpreadsheetManager::BAD_REQUEST || errType == SpreadsheetManager::INVALID_CREDENTIALS || errType == SpreadsheetManager::USER_PERM_PROBLEMS)
    {
        qCritical() << "probably wrong app logic!!!";
        return;
    }
    if (errType == SpreadsheetManager::DAILY_LIMIT)
    {
        qCritical() << "daily limit exceed!!! Can't carry on normal work!";
        endProcessing();
        return;
    }
    if (errType == SpreadsheetManager::FILE_NOT_FOUND)
    {
        qCritical() << "user have deleted file or wrong app logic";
        return;
    }
    if (errType == SpreadsheetManager::SELF_TIMEOUT_REACH)
    {
        qCritical() << "probably very bad connection";
        return;
    }
    if (errType == SpreadsheetManager::JSON_ERROR)
    {
        qCritical() << "probably wrong logic or wrong google api work";
        return;
    }


}

void NetworkManager::checkFolder()
{
    qInfo() << "CORE: check folder" ;
    m_spreadsheetAPI->getFolderOnDrive(m_googleDriveFolder);
    //m_spreadsheetAPI->getFolderOnDrive(m_googleAdditinalDriveFolder);
}

void NetworkManager::timerRestartExponental()
{
    if (m_backofStepNumber == m_maxPowExpBackof)
    {
        qCritical() << "NETWORK: exponentiona back off reech limit!";
        return;
    }

    m_procesTimer.stop();
    m_backofStepNumber++;
    m_procesTimer.setInterval(m_procesTimer.interval()*2 + (qrand() % m_widthRandom));

    qInfo() << "NETWORK: timer restart with interval = " << m_procesTimer.interval();

    m_procesTimer.start();
}

void NetworkManager::timerRestartInital()
{
    m_procesTimer.stop();

    m_backofStepNumber=1;
    m_procesTimer.setInterval(m_processEveryMs + (qrand() % m_widthRandom));

    qInfo() << "NETWORK: timer start with interval = " << m_procesTimer.interval();

    m_procesTimer.start();

}

SpreadsheetQueue *NetworkManager::createQueue(QString skype_id, QString googleSheetId)
{
    SpreadsheetQueue* queue = new SpreadsheetQueue(this,m_spreadsheetAPI,skype_id,m_accessToken,googleSheetId);
    connect(queue,SIGNAL(contactOrGroupAdded(QString,QString,bool)),this,SIGNAL(contactOrGroupAdded(QString,QString,bool)));
    connect(queue,SIGNAL(messagesAdded(QString,QDateTime,QString)),this,SIGNAL(messagesAdded(QString,QDateTime,QString)));
    connect(queue,SIGNAL(errorGoogleApi(SpreadsheetManager::ErrorType,SpreadsheetManager::RequestType,QMap<QString,QString>,QString)),this,SLOT(errorHandler(SpreadsheetManager::ErrorType,SpreadsheetManager::RequestType,QMap<QString,QString>,QString)));
    m_queues.insert(skype_id,queue);

    return queue;
}

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QSystemTrayIcon>
#include "skypeaccountwidgets.h"
#include "settings.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MainWindow(bool isFirst,QWidget *parent = 0);
    ~MainWindow();
signals:
    void doAuthorize();
    //void closeApp();
    void showMessage(QString title,QString message);
    void updateSettings();
    void openGoogleDrive();
public slots:
    void showWindow();    
    void googleDriveAuthorized();
    void googleDriveDeniedAuthorization();
    void googleDriveauthProblem();
    void refreshGUI();
private slots:
    //void editAccounts();
    void accountsAccepted();
    void applySettings();
protected:
    void closeEvent(QCloseEvent *event);
private:
    QString accountsName(QStringList names) const;

    void setGUIAccountsName(QStringList names);

    Ui::MainWindow *ui;
    Settings m_settings;
    SkypeAccountWidgets m_dialogAccounts;
    bool m_isFirst;
};

#endif // MAINWINDOW_H

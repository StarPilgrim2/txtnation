#ifndef COMMON_H
#define COMMON_H


#include <QDebug>
#include <QDateTime>
#include <QList>
#include <QPair>

#define APP_NAME       "Skype Tools"
#define APP_INNER_NAME "txtNation"
#define APP_SKYPE_NAME "Skype"


#define VERSION_MAJOR  0
#define VERSION_MINOR  9

#ifndef BUILD_NUMBER
#define BUILD_NUMBER  3
#endif

QString appDataPath();

QString logsPath();

QString appVersion();

//first is list of skype_id, second is list of paths
QPair<QStringList,QStringList> locateSkypeMainDb();

int defaultNetworkInterval();

//qInfo noQuotesDebug();

//#define qInfo() noQuotesDebug() << __FILE__ << ":" << __LINE__ << " " << __FUNCTION__ << QDateTime::currentDateTime().toString("hh:mm:ss")
#define O_ASSERT(cond) ((!(cond)) ? qWarning("ASSERT: \"%s\" in file %s, line %d",#cond, __FILE__, __LINE__) : qt_noop())


void initLog();


struct Message
{
    virtual ~Message()
    {

    }

    QString From;
    QDateTime Date;
    QString Text;

    virtual QStringList toList() const;

    virtual QDateTime KeyDate() const;
};


struct EditedMessage: public Message
{
    QString newText;
    QDateTime dateOfEdit;

    QStringList toList() const;
    QDateTime KeyDate() const;
};

struct DeletedMessage: public Message
{
    QDateTime dateOfDeletion;

    QStringList toList() const;
    QDateTime KeyDate() const;
};

template<typename T>
class ListOfMessage : public QList<T>
{
    QDateTime lastDate() const
    {
       if (this->isEmpty())
           return QDateTime();

       return this->last().KeyDate();
    }

};

typedef QList<Message> ListMessage;
typedef QList<EditedMessage> ListEditedMessage;
typedef QList<DeletedMessage> ListDeletedMessage;


template <typename T>
class MapStringToList : public QMap<QString,T>
{
    void update(const MapStringToList<T>& other)
    {
       typename MapStringToList<T>::const_iterator it =  other.begin();

       while (it != other.end())
       {
           typename MapStringToList<T>::iterator it2 = find(it.key());

           if (it2 == this->end())
               insert(it.key(),it.value());
           else
               it2.value().append(it.value);

           it++;
       }
    }
};


typedef MapStringToList<ListMessage> DictOfMessage;
typedef MapStringToList<ListEditedMessage> DictOfEditedMessage;
typedef MapStringToList<ListDeletedMessage> DictOfDeletedMessage;



#endif // COMMON_H


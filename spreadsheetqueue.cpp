#include "spreadsheetqueue.h"
#include "common.h"

SpreadsheetQueue::SpreadsheetQueue(QObject *parent,SpreadsheetManager* spreadsheet, QString skypeId, QString accessToken, const QString& googleSheetID) : QObject(parent),m_skypeId(skypeId),m_needAddSpreadsheet(false),m_spreadsheet(spreadsheet),m_activeRequest(false),m_googleSheetId(googleSheetID),m_spreadSheetIscontact(false),m_needCheckFileCreate(false)
{
    connect(m_spreadsheet,SIGNAL(errorGoogleApi(SpreadsheetManager::ErrorType,SpreadsheetManager::RequestType,QMap<QString,QString>,QString)),this,SLOT(error(SpreadsheetManager::ErrorType,SpreadsheetManager::RequestType,QMap<QString,QString>,QString)));
    connect(m_spreadsheet,SIGNAL(fileCreated(QString,QString,bool)),this,SLOT(fileCreated(QString,QString,bool)));
    connect(m_spreadsheet,SIGNAL(valuesAdded(QString,QString)),this,SLOT(valuesAdded(QString,QString)));

    m_spreadsheet->setAccessToken(accessToken);
}

void SpreadsheetQueue::addSpreadSheet(QString name,bool isContact)
{
    qInfo() << "NETWORK: add new spreadsheet for skype" << m_skypeId <<  ", name is " << name << "isContact? is" << isContact;
    m_needAddSpreadsheet = true;
    m_spreadSheetName = name;
    m_spreadSheetIscontact=isContact;

    //processQueue();
}

void SpreadsheetQueue::addMessages(QList<QStringList> data, QDateTime when)
{
    qInfo() << "NETWORK: add new spreadsheet for skype" << m_skypeId << "message count is" << data.count();
    m_queue.append(qMakePair(data,when));

    //processQueue();
}

void SpreadsheetQueue::setAccessToken(QString token)
{
    m_spreadsheet->setAccessToken(token);

    //processQueue();
}

void SpreadsheetQueue::setSpreadSheetId(QString sheetId)
{
   m_googleSheetId =  sheetId;
}

void SpreadsheetQueue::error(SpreadsheetManager::ErrorType errType,SpreadsheetManager::RequestType reqType,QMap<QString,QString> metaParam,QString errorText)
{    
    QString skype_id = metaParam.value("uid");

    if (m_skypeId != skype_id)
        return;

    //qInfo() << skype_id << error << type;

    //if erron on creating before rectreate check real situation
    if (reqType == SpreadsheetManager::CreateSpreadsheet )
    {
        qInfo() << "NETWORK: for skype" << m_skypeId << "error creation file and  need check spreadsheet creation";
        m_needCheckFileCreate = true;
        m_needAddSpreadsheet = false;
    }

    if (reqType == SpreadsheetManager::GetSpreadsheet)
    {
       qInfo() << "NETWORK: error get file for skype" << m_skypeId << " need create spreadsheet";
       m_needAddSpreadsheet = true;
       m_needCheckFileCreate = false;
    }

    m_activeRequest = false;

    emit errorGoogleApi(errType,reqType,metaParam,errorText);

    //processQueue();
}

void SpreadsheetQueue::fileCreated(QString skype_id, QString googlefileId,bool isContact)
{
    if (m_skypeId != skype_id)
        return;

    qInfo() << "NETWORK: for skype " << skype_id << "spreadsheet is successful created";

    m_activeRequest = false;
    m_needAddSpreadsheet=false;
    m_needCheckFileCreate=false;
    m_spreadSheetName.clear();

    setSpreadSheetId(googlefileId);

    emit contactOrGroupAdded(skype_id,googlefileId,isContact);

    //processQueue();
}

void SpreadsheetQueue::valuesAdded(QString skype_id, QString actualRange)
{    
    if (m_skypeId != skype_id)
        return;

    qInfo() << "NETWORK: for skype " << skype_id << "data is successful appended";

    m_activeRequest = false;
    QPair<QList<QStringList>,QDateTime> temp =  m_queue.takeFirst();

    qInfo() << "NETWORK: for skype " << skype_id << "removed first element from queue. Current queue size is" << m_queue.count();

    emit messagesAdded(skype_id,temp.second,actualRange);

    //processQueue();
}

void SpreadsheetQueue::processQueue()
{
    //a lot of strings!
    //qInfo() << "NETWORK: for skype " << m_skypeId << "queue is processed";

    //no actions or active action exist or no google access token
    if (m_activeRequest || (m_queue.isEmpty() && !m_needAddSpreadsheet) || !m_spreadsheet->accessTokenExist())
    {
        //qInfo() << "NETWORK: for skype " << m_skypeId << "nothing to do";
        return;
    }

    m_activeRequest = true;

    if (m_needAddSpreadsheet)
    {
        qInfo() << "NETWORK: for skype " << m_skypeId <<  "add spreadsheet " << m_spreadSheetName;
        m_spreadsheet->addSpreadSheet(m_spreadSheetName,m_skypeId,m_spreadSheetIscontact);
        return;
    }
    if (m_needCheckFileCreate)
    {
        qInfo() << "NETWORK: for skype " << m_skypeId <<  "check spreadsheet " << m_spreadSheetName;
        m_spreadsheet->getSpreadSheet(m_spreadSheetName,m_skypeId,m_spreadSheetIscontact);
        return;
    }

    qInfo() << "NETWORK: for skype " << m_skypeId <<  "append values to spreadsheet " << m_spreadSheetName;
    m_spreadsheet->addValuesToSheet(m_googleSheetId,m_queue.first().first,m_skypeId);
}

bool SpreadsheetQueue::isActiveRequest() const
{
    return m_activeRequest;
}

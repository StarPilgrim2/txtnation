#include <QApplication>
#include <QThread>
#include <QFile>
#include <QSysInfo>
#include <QMessageBox>
#include "common.h"
#include "corelogic.h"
#include "settings.h"
#include "mainwindow.h"
#include "skypeaccountwidgets.h"
#include "systemtray.h"
#include "apllication.h"
#include "systeminfo.h"
//#include "com/skype4comlib.h"

void printSystemInfo(const SystemInfo& info)
{
    //ODebug() << "";
    //QString infoStr = "System information:\n";

    qInfo() << info.OsVersion();
    qInfo() << info.processorBrand();
    qInfo() << info.memoryInfo();
    qInfo() << "default interface: " << info.activeInterface();

    const QStringList& interfaces = info.networkInterfaces();

    foreach (QString interfac,interfaces)
    {
        if (interfac == info.activeInterface())
            continue;
        qInfo() << "interface: " << interfac;
    }

//    int primaryNum = info.primaryScreen();

//    const QList<QRect>& resolutions = info.resolutions();

//    int i=0;
//    foreach (QRect rect,resolutions)
//    {
//        infoStr += "resolution of ";
//        if (i == primaryNum)
//            infoStr +=  "primary ";
//        infoStr +=("monitor = " + QString::number(rect.width()) + "x" + QString::number(rect.height()) + "\n");

//        i++;
//    }

}



void printInfo(const Settings& settings)
{
    qInfo() << "app was started";
    qInfo() << "App vers:" << appVersion();
    qInfo() << "First start:" << settings.firstStart();
    qInfo() << "All skype profiles:" << locateSkypeMainDb();
    qInfo() << "Interested paths:" << settings.interestedMainDbs();
    qInfo() << "Upload timeout:" << settings.uploadInterval() << "secs";

    printSystemInfo(SystemInfo::instance());
}


int main(int argc, char *argv[])
{
    Apllication app(argc, argv,"7485c713-3ac9-4e43-8900-7928e74524b5");

    if (app.isRunning())
        return 0;

//    SKYPE4COMLib::Skype skype;


//    skype.Attach();

//   qDebug() <<  skype.Version();


//   SKYPE4COMLib::IUserCollection* pUsers =  skype.Friends();

//   if (pUsers)
//   {
//       int count = pUsers->Count();
//       int i = 1;

//       while (i <= count)
//       {
//           qDebug() << pUsers->Item(i++)->FullName();
//       }

//   }




//    return 0;

    initLog();

    Settings settings;

    printInfo(settings);


    QFile styleFile(":/gui/css/common.css");

    styleFile.open(QFile::ReadOnly);

    QString style = styleFile.readAll();
    styleFile.close();

    app.setStyleSheet(style);


    bool isFirst = settings.firstStart();

    if (isFirst )
    {
        QPair<QStringList, QStringList> temp = locateSkypeMainDb();

        int count = temp.first.count();

        if ( count > 1)
        {
            SkypeAccountWidgets widg(true);

            widg.exec();

        }
        else if (count <1)
        {
            QMessageBox::information(0,QObject::tr("Information"),QObject::tr("Couldn't find any Skype profiles. Please, ensure what Skype have been installed and configure Skype profile in settings"));
        }
        else
        {
            settings.setInterestedMainDbs(temp.second,temp.first);
        }

        settings.setNonFirstStart();
    }


    SystemTray tray;
    MainWindow window(isFirst);
    window.setWindowTitle(APP_NAME);
    CoreLogic* logic = new CoreLogic();
    CoreThread coreThread(logic);




    QObject::connect(&tray,SIGNAL(showGui()),&window,SLOT(showWindow()));
    QObject::connect(&tray,SIGNAL(quitApp()),logic,SLOT(finishWork()));
    QObject::connect(logic,SIGNAL(googleDriveAuthorized()),&window,SLOT(googleDriveAuthorized()),Qt::QueuedConnection);
    QObject::connect(logic,SIGNAL(authDenied()),&window,SLOT(googleDriveDeniedAuthorization()),Qt::QueuedConnection);
    QObject::connect(logic,SIGNAL(authProblem()),&window,SLOT(googleDriveauthProblem()),Qt::QueuedConnection);
    QObject::connect(logic,SIGNAL(refreshGUI()),&window,SLOT(refreshGUI()),Qt::QueuedConnection);
    QObject::connect(logic,SIGNAL(hideGUI()),&window,SLOT(hide()),Qt::QueuedConnection);
    QObject::connect(logic,SIGNAL(hideGUI()),&tray,SLOT(hide()),Qt::QueuedConnection);

    QObject::connect(&window,SIGNAL(doAuthorize()),logic,SLOT(doAuthorize()),Qt::QueuedConnection);
    QObject::connect(&window,SIGNAL(updateSettings()),logic,SLOT(updateSettings()),Qt::QueuedConnection);
    QObject::connect(&window,SIGNAL(showMessage(QString,QString)),&tray,SLOT(showMessage(QString,QString)),Qt::QueuedConnection);
    QObject::connect(&window,SIGNAL(openGoogleDrive()),logic,SLOT(openGoogleDrive()));

    QObject::connect(&app,SIGNAL(aboutToQuit()),logic,SLOT(finishWork()),Qt::QueuedConnection);
    QObject::connect(&app,SIGNAL(commitDataRequest(QSessionManager&)),&tray,SIGNAL(quitApp()));

    window.setVisible(false);
    tray.setVisible(true);


    QMetaObject::invokeMethod(logic,"doAuthorize",Qt::QueuedConnection);

    return app.exec();
}

#include "systemtray.h"
#include "common.h"


SystemTray::SystemTray(QObject *parent):QSystemTrayIcon(parent)
{

   setIcon(QPixmap(":/gui/images/logo.png"));
   m_menu.addAction(tr("More details"),this,SIGNAL(showGui()));
   m_menu.addAction(tr("About"),this,SIGNAL(showAbout()));
   m_menu.addAction(tr("Quit"),this,SIGNAL(quitApp()));

   setContextMenu(&m_menu);

   connect(this,&QSystemTrayIcon::activated,[this](QSystemTrayIcon::ActivationReason reason){if (reason == QSystemTrayIcon::Trigger) emit showGui();});
}

void SystemTray::showMessage(QString title, QString message)
{
    QSystemTrayIcon::showMessage(title,message);
}

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QCloseEvent>
#include <QDebug>

MainWindow::MainWindow(bool isFirst, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWindow),m_dialogAccounts(false),m_isFirst(isFirst)
{
    ui->setupUi(this);
    connect(ui->closeButton,SIGNAL(clicked(bool)),this,SLOT(close()));
    connect(ui->authorizeButton,SIGNAL(clicked(bool)),this,SIGNAL(doAuthorize()));
    connect(ui->applyButton,SIGNAL(clicked(bool)),this,SLOT(applySettings()));
    connect(&m_dialogAccounts,SIGNAL(updateInterestedNames()),this,SLOT(accountsAccepted()));
    connect(ui->skypeUsersButton,SIGNAL(clicked(bool)),&m_dialogAccounts,SLOT(exec()));
    connect(ui->openDriveButton,SIGNAL(clicked(bool)),this,SIGNAL(openGoogleDrive()));



    ui->gDriveStatus->setTextFormat(Qt::RichText);
    ui->gDriveStatus->setText(tr("awaiting authorization"));
    ui->syncFrequencyspinBox->setMinimum(1);
    ui->syncFrequencyspinBox->setMaximum(60);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showWindow()
{
    qInfo() << "GUI: show";

    showNormal();

    ui->syncFrequencyspinBox->setValue(m_settings.uploadInterval() / 60);
    QDateTime last = m_settings.lastSuccesfulRequestDate();

    if (last.isValid())
        ui->lastSyncLabel->setText(last.toString());
    else
        ui->lastSyncLabel->setText("don't exist");

    QStringList interestednames = m_settings.interestedMainDbsNames();


    if (interestednames.isEmpty())
        ui->skypeUsers->setText(tr("select the account in which you are interested"));
    else
        //ui->skypeUsers->setText(accountsName(interestednames));
        setGUIAccountsName(interestednames);

    ui->versLabel->setText(appVersion());

    activateWindow();
}

void MainWindow::googleDriveAuthorized()
{
    qInfo() << "GUI: authorization is received";
    ui->gDriveStatus->setText(tr("<font color=green>authorized</font>"));
    ui->authorizeButton->setVisible(false);
}

void MainWindow::googleDriveDeniedAuthorization()
{
    qInfo() << "GUI: authorization is denied";
    ui->gDriveStatus->setText(tr("<font color=red>authorization denied</font>"));
}

void MainWindow::googleDriveauthProblem()
{
    qInfo() << "GUI: authorization is problem";
    ui->gDriveStatus->setText(tr("<font color=red>there are some errors</font>"));
}

void MainWindow::refreshGUI()
{
    qInfo() << "GUI: refresh byy logic";
    ui->lastSyncLabel->setText(m_settings.lastSuccesfulRequestDate().toString());
}

//void MainWindow::editAccounts()
//{
//    m_dialogAccounts.exec();
//}

void MainWindow::accountsAccepted()
{
    qInfo() << "GUI: accounts accept clicked";
    setGUIAccountsName(m_dialogAccounts.interestedNames());
    //ui->skypeUsers->setText(accountsName(m_dialogAccounts.interestedNames()));

}

void MainWindow::applySettings()
{
    qInfo() << "GUI: apply settings is clicked";

    QStringList oldPaths = m_settings.interestedMainDbs();
    int oldTimeout       = m_settings.uploadInterval();

    QStringList newPathes = m_dialogAccounts.interestedPathes();

    if (oldTimeout == 60*ui->syncFrequencyspinBox->value() && oldPaths == newPathes)
        return;

    qInfo() << "GUI: settigs were changed";


    m_settings.setUploadInterval(60*ui->syncFrequencyspinBox->value());
    m_settings.setInterestedMainDbs(newPathes,m_dialogAccounts.interestedNames());

    m_dialogAccounts.updateModel();

    emit updateSettings();

}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (event && isVisible())
    {
        hide();
        event->ignore();

        if (m_isFirst)
        {
            emit showMessage(tr("Systray"),tr("The program will keep running in the "
                                             "system tray. To terminate the program, "
                                             "choose <b>Quit</b> in the context menu "
                                             "of the system tray entry."));
        }

        return;
    }

    //QWidget::closeEvent(event);
}

QString MainWindow::accountsName(QStringList names) const
{
    QString disp = names.value(0);

    if (names.count() > 1)
        disp += tr(" and more ") + QString::number(names.count()-1) + tr(" account");
    if (names.count() > 2)
        disp += tr("s");

    return disp;
}

void MainWindow::setGUIAccountsName(QStringList names)
{
      QString disp = accountsName(names);

      QFontMetrics fm = ui->skypeUsers->fontMetrics();

      if (fm.width(disp) > ui->skypeUsers->width())
      {
          ui->skypeUsers->setText(fm.elidedText(disp,Qt::ElideRight,ui->skypeUsers->width()));
          ui->skypeUsers->setToolTip(disp);
      }
      else
      {
          ui->skypeUsers->setText(disp);
          ui->skypeUsers->setToolTip(QString());
      }
}

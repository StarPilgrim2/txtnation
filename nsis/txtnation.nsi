﻿;Setup Script
;--------------------------------

!define VERSION '0.9'
!ifndef BUILD 
!define BUILD '3'
!endif
!define PRODUCT_NAME "Skype Tools"
!define PRODUCT_PUBLISHER "txtNation"
!define PRODUCT_WEB_SITE "http://www.txtnation.com/"

;--------------------------------
;Configuration

OutFile txtnation-${VERSION}.${BUILD}-win.exe
SetCompressor /SOLID lzma
WindowIcon off
!define MUI_ICON "..\images\txtnation.ico"
Icon "..\images\txtnation.ico"
;XPStyle on
InstallDir "$PROGRAMFILES\txtNation"
!define COMPANY "txtNation"
!define URL "http://www.txtnation.com/"

!define UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\txtNation"
;--------------------------------
;Variables

  Var MUI_TEMP
  Var STARTMENU_FOLDER

;--------------------------------
;Header Files

!include "LogicLib.nsh"
!define MULTIUSER_INSTALLMODE_INSTDIR "txtnation"
!define MULTIUSER_INSTALLMODE_INSTDIR_REGISTRY_KEY "Software\txtNation"
!define MULTIUSER_INSTALLMODE_INSTDIR_REGISTRY_VALUE "Start_Menu_Folder"
!define  MULTIUSER_EXECUTIONLEVEL Highest
!define  MULTIUSER_MUI
!include "MultiUser.nsh"
!include "MUI2.nsh"
!include "EnvVarUpdate.nsh"
!include "Sections.nsh"
!include "nsDialogs.nsh"
!include "FileFunc.nsh"

;--------------------------------
;Configuration

;Names
Name "${PRODUCT_NAME}"
Caption "${PRODUCT_NAME} ${VERSION}.${BUILD} Setup"

;Page custom myWelcomePage myWelcomePageLeave

;Interface Settings
!define MUI_ABORTWARNING

!define MUI_HEADERIMAGE
!insertmacro MUI_DEFAULT MUI_HEADERIMAGE_BITMAP "..\images\logo.bmp"

!define MUI_WELCOMEFINISHPAGE_BITMAP "..\images\big_logo.bmp"

!define MUI_COMPONENTSPAGE_SMALLDESC

;Pages
!define MUI_WELCOMEPAGE_TITLE "Welcome to ${PRODUCT_NAME} ${VERSION}.${BUILD} installation wizard"
!define MUI_WELCOMEPAGE_TEXT "$\r$\n$\r$\n$_CLICK"
!insertmacro MUI_PAGE_WELCOME

!define MULTIUSER_INSTALLMODEPAGE_TEXT_CURRENTUSER "Install jist for me (recommended)"
!define MULTIUSER_INSTALLMODE_DEFAULT_CURRENTUSER
!insertmacro MULTIUSER_PAGE_INSTALLMODE


;!insertmacro MUI_PAGE_LICENSE "COPYING"

;!insertmacro MUI_PAGE_DIRECTORY

;Start Menu Folder Page Configuration
;!define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU"
!define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\txtNation"
!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start_Menu_Folder"

!insertmacro MUI_PAGE_STARTMENU Application $STARTMENU_FOLDER

!insertmacro MUI_PAGE_INSTFILES

!define MUI_FINISHPAGE_LINK "For detailed information on products refer to our website."
!define MUI_FINISHPAGE_LINK_LOCATION "http://www.txtnation.com/"

!define MUI_FINISHPAGE_RUN_TEXT  "Run app (strongly recommended)"
!define MUI_FINISHPAGE_RUN "$INSTDIR\txtnation.exe"

!define MUI_FINISHPAGE_NOREBOOTSUPPORT

;!define MUI_FINISHPAGE_SHOWREADME_TEXT "Security recommendations"
;!define MUI_FINISHPAGE_SHOWREADME "$INSTDIR\docs\security_en.html"
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES


brandingtext $(DESC_BRANDING)

;--------------------------------
;Languages

  !insertmacro MUI_LANGUAGE "English"
;-------------------------------
  VIProductVersion "${VERSION}.0.${BUILD}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "CompanyName" "txtNation"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription" "${PRODUCT_NAME}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "FileVersion" "${VERSION}.${BUILD}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "InternalName" "txtnation-${VERSION}-${BUILD}-win.exe"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "OriginalFilename" "txtnation-${VERSION}-${BUILD}-win.exe"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductName" "${PRODUCT_NAME}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductVersion" "${VERSION}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalTrademarks" "txtNation"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalCopyright" "© txtNation, 2016"

;-------------------------------

;--------------------------------
;Installer Sections

Var welcome.Dialog
Var fast

Function .onInit
	!insertmacro MULTIUSER_INIT
FunctionEnd

Function un.onInit
  !insertmacro MULTIUSER_UNINIT
FunctionEnd


Section "${PRODUCT_NAME}" SecCore

  StrCpy $0 "txtnation.exe"
  KillProc::KillProcesses
  Sleep 1000
  ExecWait "taskkill /f /im txtnation.exe"  
  Sleep 3000

  SetDetailsPrint textonly
  DetailPrint "Installing ${PRODUCT_NAME} Files..."
  SetDetailsPrint listonly
  SectionIn 1 2 3 RO
  SetOutPath $INSTDIR
  SetOverwrite on
  
  File ..\fordeploy\txtnation.exe
  File ..\fordeploy\libgcc_s_dw2-1.dll
  File ..\fordeploy\libstdc++-6.dll
  File ..\fordeploy\libwinpthread-1.dll
  File ..\fordeploy\Qt5Core.dll
  File ..\fordeploy\Qt5Gui.dll
  File ..\fordeploy\Qt5Network.dll
  File ..\fordeploy\Qt5Sql.dll
  File ..\fordeploy\Qt5Widgets.dll
  File ..\fordeploy\ssleay32.dll
  File ..\fordeploy\libeay32.dll
  File ..\fordeploy\Qt5Widgets.dll
  
  SetOutPath "$INSTDIR\sqldrivers\"
  File ..\fordeploy\sqldrivers\qsqlite.dll
  
  SetOutPath "$INSTDIR\platforms\"  
  File ..\fordeploy\platforms\qwindows.dll 
  
  SetOutPath "$INSTDIR"
SectionEnd


Section -AdditionalIcons
  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application  
    ;Create shortcuts
    CreateDirectory "$SMPROGRAMS\$STARTMENU_FOLDER"
    CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\txtnation.lnk" "$INSTDIR\txtnation.exe" "" "$INSTDIR\txtnation.exe"
    CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\Uninstall.lnk" "$INSTDIR\Uninstall.exe"
    CreateShortCut "$DESKTOP\txtnation.lnk" "$INSTDIR\txtnation.exe" "" "$INSTDIR\txtnation.exe"

  !insertmacro MUI_STARTMENU_WRITE_END
SectionEnd

Section -post
  WriteRegStr SHCTX "${UNINST_KEY}" "DisplayName" "${PRODUCT_NAME}"
  WriteRegStr SHCTX "${UNINST_KEY}" "UninstallString" "$\"$INSTDIR\uninstall.exe$\" /$MultiUser.InstallMode"
  WriteRegStr SHCTX "${UNINST_KEY}" "QuietUninstallString" "$\"$INSTDIR\uninstall.exe$\" /$MultiUser.InstallMode /S"
  
  WriteRegStr SHCTX "Software\Microsoft\Windows\CurrentVersion\Run" "txtNation" "$INSTDIR\txtnation.exe"
	
  WriteUninstaller $INSTDIR\Uninstall.exe

  SetDetailsPrint both
  ;IfSilent 0 +6
  
  ;KillProcDLL::KillProc "txtnation.exe"
  ;Sleep 300
  
  
  ;${If} $option_postStart == 1
  ;  Exec $INSTDIR\txtnation.exe
  ;${EndIf}

SectionEnd


;--------------------------------
;Uninstaller Section

Section Uninstall
  StrCpy $0 "txtnation.exe"
  KillProc::KillProcesses
  Sleep 1000
  ExecWait "taskkill /f /im txtnation.exe"  
  Sleep 3000
  
  ;FindWindow $0 "txtNation"
  ;SendMessage $0 ${WM_QUIT} 0 0
  
  
  SetDetailsPrint textonly
  DetailPrint "Uninstalling ${PRODUCT_NAME}..."
  SetDetailsPrint listonly

  RMDir /r "$INSTDIR"
  RMDir /r "$INSTDIR"
  
  RMDir /r "$APPDATA\txtnation"
  RMDir /r "$APPDATA\txtnation"

  !insertmacro MUI_STARTMENU_GETFOLDER Application $MUI_TEMP
  
  Delete "$SMPROGRAMS\$MUI_TEMP\txtnation.lnk"
  Delete "$SMPROGRAMS\$MUI_TEMP\Uninstall.lnk"
  RMDir "$SMPROGRAMS\$MUI_TEMP"
  Delete "$DESKTOP\txtnation.lnk"
  DeleteRegKey SHCTX "${UNINST_KEY}"
  DeleteRegValue  SHCTX "Software\Microsoft\Windows\CurrentVersion\Run" "txtNation"
  SetDetailsPrint both

SectionEnd


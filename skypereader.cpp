#include "skypereader.h"
#include "common.h"
#include <QSqlQuery>
#include <QSqlError>
#include <QUuid>


static const QString selectnormalContacts("Select Contacts.skypename, Contacts.displayname  from Contacts where Contacts.revoked_auth is Null and Contacts.given_authlevel=1 and Contacts.skypename != 'echo123' and Contacts.skypename != '28:concierge';");
static const QString selectGroupchatNames("Select Conversations.identity, Conversations.displayname  from Conversations where Conversations.last_activity_timestamp is not Null and Conversations.type=2;");
static const QString selectConvoIdforContacnts("Select p.identity, conv.id  from Contacts as c inner join Participants as p on p.identity = c.skypename inner join Conversations as conv on conv.id = p.convo_id  where conv.type=1 and c.revoked_auth is Null and c.given_authlevel=1 and c.skypename != 'echo123' and c.skypename != '28:concierge';");
static const QString selectGroupConvoId("Select Conversations.identity, Conversations.id  from Conversations where Conversations.last_activity_timestamp is not Null and Conversations.type=2;");
static const QString selectMessagesByConvoIdAndDate("Select Messages.from_dispname, Messages.timestamp, Messages.body_xml from Messages where Messages.convo_id=:convo_id and Messages.type=61 and (Messages.timestamp > :timestamp or :timestamp isNull) ORDER BY Messages.timestamp;");
static const QString selectEditedMessagesByConvoIdAndDate("Select Messages.from_dispname, Messages.timestamp, Messages.body_xml, Messages.edited_timestamp from Messages where Messages.convo_id=:convo_id and Messages.type=61 and (Messages.edited_timestamp > :edited_timestamp or :edited_timestamp isNull) ORDER BY Messages.timestamp;");
static const QString selectDeletedMessagesByConvoIdAndDate("Select Messages.from_dispname, Messages.timestamp, Messages.edited_timestamp from Messages where Messages.convo_id=:convo_id and Messages.type=61 and (Messages.edited_timestamp > :edited_timestamp or :edited_timestamp isNull) AND Messages.body_xml='' ORDER BY Messages.timestamp;");
static const QString selectAccount("Select Accounts.signin_name, Accounts.fullname from Accounts LIMIT 1;");


class SqliteLocker
{
public:
    SqliteLocker(QSqlDatabase connection):m_connection(connection),m_okBeginTransaction(false)
    {
//        m_okBeginTransaction = m_connection.transaction();

//        if (!m_okBeginTransaction)
//        {
//            QSqlError err = m_connection.lastError();

//            qDebug() << err.databaseText() << err.driverText() << err.text();
//        }
    }

    ~SqliteLocker()
    {
        //m_connection.commit();
//        bool NoInternalError = true;

//        if (isOk())
//           NoInternalError = m_connection.commit();
//        else if (m_okBeginTransaction)
//        {
//           QSqlError err = m_connection.lastError();

//           qDebug() << err.databaseText() << err.driverText() << err.text();

//           NoInternalError = m_connection.rollback();
//        }

//        if (!NoInternalError)
//        {
//            QSqlError err = m_connection.lastError();

//            qCritical() << err.databaseText() << err.driverText() << err.text();
//        }

    }

    bool isOk()
    {
        return true;
        //return m_okBeginTransaction && !m_connection.lastError().isValid();
    }

    QSqlDatabase& m_connection;
    bool m_okBeginTransaction;
};

#define LOCK_AND_CKECK(res) SqliteLocker temp123(m_connection); if (!temp123.isOk()) return res;


SkypeReader::SkypeReader(QString path, QObject *parent) : QObject(parent)
{    
    m_connection = QSqlDatabase::addDatabase("QSQLITE",path + QUuid::createUuid().toString());

    m_connection.setDatabaseName(path);
    //m_connection.setConnectOptions("QSQLITE_OPEN_READONLY");

    if (!m_connection.open())
    {
        qCritical() << m_connection.lastError();
    }
    else
    {
        qInfo() << "connect to " << path;
    }

    QSqlQuery temp("SELECT sqlite_version();",m_connection);

    temp.exec();

    temp.first();

    qDebug() << temp.value(0).toString();

    applyPragma(QStringList() << "journal_mode=PERSIST" << "secure_delete=1" << "encoding='UTF-8'" << "page_size=4096" << "synchronous=2" << "synchronous=FULL");

//    QSqlQuery query("PRAGMA journal_mode = PERSIST;",m_connection);

//    if(!query.exec())
//    {
//        qCritical() <<  query.lastError();
//    }
//    else
//    {
//        qInfo() << "PRAGMA journal_mode = PERSIST ";
//    }

//    QSqlQuery query2("PRAGMA synchronous=FULL;",m_connection);

//    if(!query2.exec())
//    {
//        qCritical() <<  query2.lastError();
//    }
//    else
//    {
//        qInfo() << "PRAGMA synchronous=FULL;";
//    }
}

SkypeReader::~SkypeReader()
{
    m_connection.close();

    QSqlDatabase::removeDatabase(m_connection.databaseName());
}

QPair<QString, QString> SkypeReader::account() const
{
    QPair<QString, QString> res;

    LOCK_AND_CKECK(res)

    QSqlQuery query(m_connection);

    if (!query.exec(selectAccount))
    {
        qCritical() << query.lastError();
        return res;
    }

    while (query.next())
    {
        res.first  = query.value(0).toString();
        res.second = query.value(1).toString();
    }

    return res;

}

QMap<QString,QString>  SkypeReader::normalContactList() const
{
   QMap<QString,QString> res;

   LOCK_AND_CKECK(res);

   QSqlQuery query(m_connection);

   if (!query.exec(selectnormalContacts))
   {
       qCritical() << query.lastError();
       return res;
   }

   while (query.next())
       res[query.value(0).toString()] = query.value(1).toString();

   return res;

}

QMap<QString, QString> SkypeReader::groupchatList() const
{
    QMap<QString, QString> res;

    LOCK_AND_CKECK(res);

    QSqlQuery query(m_connection);

    if (!query.exec(selectGroupchatNames))
    {
        qCritical() << query.lastError();
        return res;
    }

    while (query.next())
        res[query.value(0).toString()] = query.value(1).toString();

    return res;

}

 bool SkypeReader::newMessages(const QMap<QString,QDateTime> &afterThan,QMap<QString,QList<QStringList>>& res/*,QMap<QString,bool>& updated*/ )
{
    //QMap<QString,QList<QStringList>> res;

    //LOCK_AND_CKECK(res);

//    if (!m_connection.transaction())
//    {
//        QSqlError err = m_connection.lastError();

//        qDebug() << err.databaseText() << err.driverText() << err.text();
//        return false;
//    }

    QSqlQuery contactQuery(m_connection);

    if (!contactQuery.exec(selectConvoIdforContacnts))
    {
        qCritical() << contactQuery.lastError();

        //m_connection.rollback();

        return false;
    }

   // m_connection.commit();

    while (contactQuery.next())
    {
        QString skype_id = contactQuery.value(0).toString();
        QString convo_id = contactQuery.value(1).toString();

        QList<QStringList> temp;

        if (newMessages(convo_id,afterThan.value(skype_id),temp) && !temp.isEmpty())
            res[skype_id] = temp;
    }


//    if (!m_connection.transaction())
//    {
//        QSqlError err = m_connection.lastError();

//        qDebug() << err.databaseText() << err.driverText() << err.text();
//        return false;
//    }
    QSqlQuery groupQuery(m_connection);

    if (!groupQuery.exec(selectGroupConvoId))
    {
        qCritical() << groupQuery.lastError();

        //m_connection.rollback();
        return false;
    }

    //m_connection.commit();

    while (groupQuery.next())
    {
        QString group_id = groupQuery.value(0).toString();
        QString convo_id = groupQuery.value(1).toString();

        QList<QStringList> temp;

        if (newMessages(convo_id,afterThan.value(group_id),temp) && !temp.isEmpty())
            res[group_id] = temp;
    }

    return true;

 }

 bool SkypeReader::newEditedMessages(const QMap<QString,QDateTime> &afterThan,QMap<QString,ListEditedMessage>& res)
 {

     QSqlQuery contactQuery(m_connection);

     if (!contactQuery.exec(selectConvoIdforContacnts))
     {
         qCritical() << contactQuery.lastError();

         return false;
     }

     while (contactQuery.next())
     {
         QString skype_id = contactQuery.value(0).toString();
         QString convo_id = contactQuery.value(1).toString();

         ListEditedMessage temp;

         if (newEditedMessages(convo_id,afterThan.value(skype_id),temp) && !temp.isEmpty())
             res[skype_id] = temp;
     }

     QSqlQuery groupQuery(m_connection);

     if (!groupQuery.exec(selectGroupConvoId))
     {
         qCritical() << groupQuery.lastError();

         return false;
     }

     while (groupQuery.next())
     {
         QString group_id = groupQuery.value(0).toString();
         QString convo_id = groupQuery.value(1).toString();

         ListEditedMessage temp;

         if (newEditedMessages(convo_id,afterThan.value(group_id),temp) && !temp.isEmpty())
             res[group_id] = temp;
     }

     return true;
 }

 bool SkypeReader::newDeletedMessages(const QMap<QString,QDateTime> &afterThan,QMap<QString,ListDeletedMessage>& res)
 {
     QSqlQuery contactQuery(m_connection);

     if (!contactQuery.exec(selectConvoIdforContacnts))
     {
         qCritical() << contactQuery.lastError();

         return false;
     }

     while (contactQuery.next())
     {
         QString skype_id = contactQuery.value(0).toString();
         QString convo_id = contactQuery.value(1).toString();

         ListDeletedMessage temp;

         if (newDeltedMessages(convo_id,afterThan.value(skype_id),temp) && !temp.isEmpty())
             res[skype_id] = temp;
     }

     QSqlQuery groupQuery(m_connection);

     if (!groupQuery.exec(selectGroupConvoId))
     {
         qCritical() << groupQuery.lastError();

         return false;
     }

     while (groupQuery.next())
     {
         QString group_id = groupQuery.value(0).toString();
         QString convo_id = groupQuery.value(1).toString();

         ListDeletedMessage temp;

         if (newDeltedMessages(convo_id,afterThan.value(group_id),temp) && !temp.isEmpty())
             res[group_id] = temp;
     }

     return true;
 }

 void SkypeReader::applyPragma(QStringList pragmas)
 {
     for (QString pragma:pragmas)
     {
          QSqlQuery query(QString("PRAGMA %1;").arg(pragma),m_connection);

          if (query.exec())
              qDebug() << query.executedQuery() << "is succesful";
          else
              qDebug() << query.executedQuery() << "is failed";
     }
 }




 bool SkypeReader::newMessages(QString convo_id, const QDateTime &afterThan,QList<QStringList>& res)
{
    //QList<QStringList>  res;
    bool ret = false;
    LOCK_AND_CKECK(ret);

    QSqlQuery query(m_connection);


    if (!query.prepare(selectMessagesByConvoIdAndDate))
    {
        qCritical() << query.lastError();
        return ret;
    }

    query.bindValue(":convo_id",convo_id);
    query.bindValue(":timestamp",afterThan.isValid()?afterThan.toTime_t():0);


    if (!query.exec())
    {
        qCritical() << query.lastError();
        return ret;
    }

    while (query.next())
    {
        res.append(QStringList() << query.value(0).toString() << QDateTime::fromTime_t(query.value(1).toInt()).toString() << query.value(2).toString());
    }

    return true;
 }

 bool SkypeReader::newEditedMessages(QString convo_id, const QDateTime &afterThan, ListEditedMessage &res)
 {
     bool ret = false;
     LOCK_AND_CKECK(ret);

     QSqlQuery query(m_connection);


     if (!query.prepare(selectEditedMessagesByConvoIdAndDate))
     {
         qCritical() << query.lastError();
         return ret;
     }

     query.bindValue(":convo_id",convo_id);
     query.bindValue(":edited_timestamp",afterThan.isValid()?afterThan.toTime_t():0);


     if (!query.exec())
     {
         qCritical() << query.lastError();
         return ret;
     }

     while (query.next())
     {
         EditedMessage mes;
         mes.From = query.value(0).toString();
         mes.Date = QDateTime::fromTime_t(query.value(1).toInt());
         mes.newText = query.value(2).toString();
         mes.dateOfEdit = QDateTime::fromTime_t(query.value(3).toInt());
         res.append(mes);
     }

     return true;
 }

 bool SkypeReader::newDeltedMessages(QString convo_id, const QDateTime &afterThan, ListDeletedMessage &res)
 {
     bool ret = false;
     LOCK_AND_CKECK(ret);

     QSqlQuery query(m_connection);


     if (!query.prepare(selectDeletedMessagesByConvoIdAndDate))
     {
         qCritical() << query.lastError();
         return ret;
     }

     query.bindValue(":convo_id",convo_id);
     query.bindValue(":edited_timestamp",afterThan.isValid()?afterThan.toTime_t():0);


     if (!query.exec())
     {
         qCritical() << query.lastError();
         return ret;
     }

     while (query.next())
     {
         DeletedMessage mes;
         mes.From = query.value(0).toString();
         mes.Date = QDateTime::fromTime_t(query.value(1).toInt());
         mes.dateOfDeletion = QDateTime::fromTime_t(query.value(2).toInt());
         res.append(mes);
     }

     return true;
 }

#include "authgoogle.h"
#include "common.h"
#include <QUrl>
#include <QUrlQuery>
#include <QNetworkRequest>
#include <QDesktopServices>
#include <QJsonDocument>
#include <QEvent>

//const QString AuthGoogle::m_clientID = "572997528684-pohp0321kja76aebuae49s0brs2dougg.apps.googleusercontent.com";
//const QString AuthGoogle::m_clientSecret = "zUv0o8XBnw8zCTARZGhgMwtP";

const QString AuthGoogle::m_clientID = "1089927786090-fpjuumie6d9li3rk74bbi0v9ainbbno6.apps.googleusercontent.com";
const QString AuthGoogle::m_clientSecret = "6PT2Pnz0eBcf3QOacm1w2W02";

const QString AuthGoogle::m_authorizationEndpoint = "https://accounts.google.com/o/oauth2/v2/auth";
const QString AuthGoogle::m_tokenEndpoint = "https://www.googleapis.com/oauth2/v4/token";
const QString AuthGoogle::m_userInfoEndpoint = "https://www.googleapis.com/oauth2/v3/userinfo";
const QString AuthGoogle::m_spreadsheetScope =  "https://www.googleapis.com/auth/spreadsheets";
const QString AuthGoogle::m_driveScope =  "https://www.googleapis.com/auth/drive";

AuthGoogle::AuthGoogle(QString refreshToken,QObject *parent) : QObject(parent),m_loopbackserver(new AuthHttpServer(this)),m_networkManager(this),m_refreshAccesTimer(this),m_refreshToken(refreshToken),m_retryTimer(this)
{
    connect(m_loopbackserver.data(),SIGNAL(authCode(QString)),this,SLOT(receivedAuthCode(QString)));
    connect(m_loopbackserver.data(),SIGNAL(authDenied()),this,SIGNAL(authDenied()));

    m_refreshAccesTimer.setSingleShot(true);
    connect(&m_refreshAccesTimer,SIGNAL(timeout()),this,SLOT(refreshAcces()));

    m_retryTimer.setSingleShot(true);
    connect(&m_retryTimer,SIGNAL(timeout()),this,SLOT(authErrorHandler()));
    m_retryTimer.setInterval(m_failbackinterval*1000);

}

void AuthGoogle::doAuth()
{
    if (m_refreshToken.isEmpty())
    {
        qInfo() << "AUTH: start auth, without refresh token";

        if (!m_loopbackserver->listen())
        {
            qInfo() << "can't listen loopback!!!";
            return;
        }

        m_loopbackUrl = m_loopbackserver->loopbackUrl();

        QUrlQuery query;

        query.addQueryItem("response_type","code");
        query.addQueryItem("client_id",m_clientID);
        query.addQueryItem("redirect_uri",m_loopbackserver->loopbackUrl());
        query.addQueryItem("scope",m_spreadsheetScope + " " + m_driveScope);
        query.addQueryItem("state","inital state");


        QUrl authUrl(m_authorizationEndpoint);
        authUrl.setQuery(query);


        qDebug() << authUrl;

        if (QDesktopServices::openUrl(authUrl))
            qInfo() << "AUTH: url was sent to browser";
        else
            qCritical() << "AUTH: can't open url";

    }
    else
    {
        qInfo() << "AUTH: start auth, WITH refresh token";

        refreshAcces();
    }

}

void AuthGoogle::receivedAuthCode(QString code)
{
    if (code.isEmpty())
    {
        qCritical() << "auth code is empty!";
        return;
    }


    m_code = code;

    m_loopbackserver->disconnect();
    m_loopbackserver.reset();

    buildTokenRequest(false,m_code);

}

void AuthGoogle::receiveToken()
{
    qInfo() << "AUTH: token response was received";

    QNetworkReply* reply = dynamic_cast<QNetworkReply*>(sender());

    if (!reply)
    {
        qCritical() << "AUTH: empty auth";
        return;
    }

    reply->disconnect();
    reply->deleteLater();
    reply->removeEventFilter(this);

    if (reply->error() != QNetworkReply::NoError)
    {
        qCritical() << "AUTH: " << reply->errorString() << reply->error();
        return;
    }

    parseToken(reply->readAll());

}

void AuthGoogle::refreshAcces()
{
    qInfo() << "AUTH: refresh token request";
    buildTokenRequest(true,m_refreshToken);
}

void AuthGoogle::parseToken(QByteArray data)
{
    //QByteArray data = m_reply->readAll();

    qDebug() << data;

    data.replace('\n',"").replace('\r',"");

    QJsonParseError jsonError;
    QVariant jsonData = QJsonDocument::fromJson(data,&jsonError).toVariant();

    if (!jsonData.isValid() || jsonError.error != QJsonParseError::NoError)
    {
        qCritical() << jsonError.errorString();
        return;
    }


    QMap<QString, QVariant> tempMap = jsonData.toMap();


    QString accessTkn = tempMap.value("access_token").toString();

    if (accessTkn.isEmpty())
    {
        qCritical() << "AUTH: access_token is empty!!!";
        return;
    }

    emit accessToken(accessTkn);

    QString tempRefreshToken = tempMap.value("refresh_token").toString();

    if (!tempRefreshToken.isEmpty())
    {
        m_refreshToken = tempRefreshToken;

        emit refreshToken(m_refreshToken);
    }
    else if (m_refreshToken.isEmpty())
    {
        qInfo() << "AUTH: refresh_token is empty!!!";
        return;
    }

    int secs = tempMap.value("expires_in").toInt();

    if (secs<=0)
    {
        qCritical() << "AUTH: expires_in is incorrect!!!";
        return;
    }

    qInfo() << "expires_in=" << secs << "secs";

    if (secs>1)
        secs--;

    //secs = 10;

    m_refreshAccesTimer.setInterval(secs*1000);
    m_refreshAccesTimer.start();
}

void AuthGoogle::initRetryOnError()
{
    qDebug() << "AUTH: error on aobtain data. Init retrying for " << m_failbackinterval << "secs";

    emit authError();
    m_retryTimer.start();
}

void AuthGoogle::authErrorHandler()
{
    if (m_refreshToken.isEmpty())
        buildTokenRequest(false,m_code);
    else
        buildTokenRequest(true,m_refreshToken);
}

void AuthGoogle::buildTokenRequest(bool refresh, QString codeOrRefreshToken)
{
    if (refresh)
        qInfo() << "AUTH: get only access token";
    else
        qInfo() << "AUTH: get refresh and access tokens";

    QNetworkRequest request;
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");
    request.setUrl(m_tokenEndpoint);

    QString requestData;

    if (!refresh)
        requestData += "code=" + codeOrRefreshToken + "&redirect_uri=" + m_loopbackUrl + "&grant_type=authorization_code";
    else
        requestData += "refresh_token=" + codeOrRefreshToken + "&grant_type=refresh_token";

    requestData += + "&client_id=" + m_clientID + "&client_secret=" + m_clientSecret ;

    qInfo() << requestData;


    QNetworkReply* reply = m_networkManager.post(request,requestData.toLatin1());

    reply->startTimer(1000*defaultNetworkInterval());
    reply->installEventFilter(this);
    reply->setProperty("ReplyObject","1");


    if (!reply)
    {
        qCritical() << "AUTH: reply is null!!!";
        return;
    }

    connect(reply,SIGNAL(finished()),this,SLOT(receiveToken()));

}


bool AuthGoogle::eventFilter(QObject * obj, QEvent * event)
{
    if (event->type() == QEvent::Timer )
    {
        if (!obj->property("ReplyObject").isNull())
        {
            QNetworkReply* reply = dynamic_cast<QNetworkReply*>(obj);

            if (reply)
            {
                qCritical() << "AUTH request time out";
                reply->disconnect();
                reply->deleteLater();

            }
        }
    }

    return QObject::eventFilter(obj,event);

}


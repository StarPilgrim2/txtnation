#include "corelogic.h"
#include "authgoogle.h"
#include "common.h"
#include "skypewatcher.h"
#include <QSqlError>
#include <QSqlQuery>
#include <QApplication>
#include <QDesktopServices>
#include <apllication.h>

const QString CoreLogic::m_driveUrl("https://drive.google.com/drive/my-drive");

CoreLogic::CoreLogic(QObject *parent) : QObject(parent),m_settings(this),m_auth(new AuthGoogle(m_settings.refreshtoken(),this)),m_accessTokenReceived(false),m_uploadTimer(this),m_abortTimer(this),m_alreadyFinished(false)
{
    qsrand(QDateTime::currentDateTime().toTime_t());

    connect(m_auth.data(),SIGNAL(accessToken(QString)),this,SLOT(changeAccessToken(QString)));
    connect(m_auth.data(),SIGNAL(refreshToken(QString)),this,SLOT(refreshTokenReceive(QString)));
    connect(m_auth.data(),SIGNAL(authDenied()),this,SIGNAL(authDenied()));
    connect(m_auth.data(),SIGNAL(authError()),this,SLOT(authError()));
    // m_auth->doAuth();authError

    m_networkApiManager.reset(new NetworkManager(m_settings.googleDriveFolder(),this));

    connect(m_networkApiManager.data(),SIGNAL(contactOrGroupAdded(QString,QString,bool)),this,SLOT(contactOrGroupAdded(QString,QString,bool)));
    connect(m_networkApiManager.data(),SIGNAL(messagesAdded(QString,QDateTime,QString)),this,SLOT(messagesAdded(QString,QDateTime,QString)));
    connect(m_networkApiManager.data(),SIGNAL(folderWasCreated(QString,QString)),this,SLOT(folderWasCreated(QString,QString)));

    //QPair<QStringList, QStringList> temp = locateSkypeMainDb();
    QStringList temp = m_settings.interestedMainDbs();

    //m_skypeAppUsers = temp.first;

    for(QString path:temp)
    {
        m_databaseConnections[path] = new SkypeWatcher(path,m_settings.contactsSkypeIds(),m_settings.groupsSkypeIds(),m_settings.lastSync(),this);
    }

    m_uploadTimer.setInterval(m_settings.uploadInterval()*1000);
    connect(&m_uploadTimer,SIGNAL(timeout()),this,SLOT(retriveData()));

    m_abortTimer.setSingleShot(true);
    m_abortTimer.setInterval(m_abortInterval*1000);
    connect(&m_abortTimer,SIGNAL(timeout()),QApplication::instance(),SLOT(quit()));

    //connect(,SIGNAL(commitDataRequest(QSessionManager &sessionManager)),this,SLOT(finishWork()));
}

CoreLogic::~CoreLogic()
{
    m_uploadTimer.stop();

    qInfo() << "CORE:" << "stop logic";
}

void CoreLogic::doAuthorize()
{
    m_auth->doAuth();
}

void CoreLogic::updateSettings()
{
    qInfo() << "CORE: apply new settings";

    bool activeUpload = m_uploadTimer.isActive();

    qInfo() << "CORE: upload timer is active?" << activeUpload;

    if (activeUpload)
    {
        qInfo() << "CORE: upload timer is stopped";
        m_uploadTimer.stop();
    }

    m_uploadTimer.setInterval(m_settings.uploadInterval()*1000);

    QSet<QString> newPats = QSet<QString>::fromList(m_settings.interestedMainDbs());

    qInfo() << "CORE: new interested pathes" << newPats;
    qInfo() << "CORE: new interval" << m_settings.uploadInterval() << "secs";

    QMap<QString,SkypeWatcher*>::iterator it =  m_databaseConnections.begin();

    while (it != m_databaseConnections.end())
    {
        if (!newPats.contains(it.key()))
        {
            qInfo() << "CORE: delete watcher " << it.key();
            it.value()->deleteLater();
            it = m_databaseConnections.erase(it);
        }
        else
        {
            it++;
        }
    }

    for(QString path:newPats)
    {
        if (!m_databaseConnections.contains(path))
        {
            SkypeWatcher* watcher =  new SkypeWatcher(path,m_settings.contactsSkypeIds(),m_settings.groupsSkypeIds(),m_settings.lastSync(),this);
            m_databaseConnections[path] = watcher;

            qInfo() << "CORE: append watcher " << path;

            if (m_accessTokenReceived)
            {
                QMap<QString, QString> contacts   = watcher->newNormalContactList();
                QMap<QString, QString> groups     = watcher->newGroupchatList();
                QMap<QString, QList<QStringList>> messages;
                QMap<QString,ListEditedMessage> editedMessages;
                QMap<QString,ListDeletedMessage> deletedMessages;
                QMap<QString, QDateTime> updatedTo;
                QMap<QString, QDateTime> lastEditedMessageDate;
                QMap<QString, QDateTime> lastDeltedMessageDate;

                if (watcher->allMessages(messages,editedMessages,deletedMessages,updatedTo,lastEditedMessageDate,lastDeltedMessageDate))
                    m_networkApiManager->addData(contacts,groups,messages,updatedTo,m_settings.googleSheetIds());
                watcher->startPeridicallyread();
            }
        }
    }

    if (activeUpload)
    {
        qInfo() << "CORE: upload timer is started";
        m_uploadTimer.start();
    }
}

void CoreLogic::finishWork()
{
    if (m_alreadyFinished)
        return;

    qInfo() << "request for end of work";

    emit hideGUI();

    m_uploadTimer.stop();

    qInfo() << "CORE: upload timer is stoped";

    if (m_networkApiManager->endProcessing())
        QApplication::instance()->quit();
    else
    {
        qInfo() << "CORE: active queries exist, finish work in " << m_abortInterval << " secs";
        m_abortTimer.start();
    }

    m_alreadyFinished = true;
}

void CoreLogic::openGoogleDrive()
{
    bool res = QDesktopServices::openUrl(m_driveUrl);

    if (res)
        qInfo() << "CORE: open google drive";
    else
        qCritical() << "CORE: can't open google drive";
}

void CoreLogic::changeAccessToken(QString token)
{
    qInfo() << "CORE: access token was received";

    m_networkApiManager->setAcessToken(token);

    if (!m_accessTokenReceived)
    {
        qInfo() << "CORE: start timer and uploading data";

        emit googleDriveAuthorized();
        m_accessTokenReceived = true;

        retriveData(true);

        m_uploadTimer.start();
    }
}

void CoreLogic::refreshTokenReceive(QString token)
{
    qInfo() << "CORE : refresh token have been received";

    m_settings.setRefreshToken(token);
}



void CoreLogic::retriveData(bool allMessages)
{
    qInfo() << "CORE: retrive all messages?" << allMessages;

    for(SkypeWatcher* watcher:m_databaseConnections.values())
    {
        if (!watcher)
            continue;

        QMap<QString, QString> contacts   = watcher->newNormalContactList();
        QMap<QString, QString> groups     = watcher->newGroupchatList();
        QMap<QString, QList<QStringList>> messages;
        QMap<QString,ListEditedMessage> editedMessages;
        QMap<QString,ListDeletedMessage> deletedMessages;
        QMap<QString, QDateTime> updatedTo;
        QMap<QString, QDateTime> lastEditedMessageDate;
        QMap<QString, QDateTime> lastDeltedMessageDate;



        bool res = false;

        if (allMessages)
        {
            res = watcher->allMessages(messages,editedMessages,deletedMessages,updatedTo,lastEditedMessageDate,lastDeltedMessageDate);
            watcher->startPeridicallyread();
        }
        else
            res = watcher->newMessages(messages,editedMessages,deletedMessages,updatedTo,lastEditedMessageDate,lastDeltedMessageDate);

        if (res)
            m_networkApiManager->addData(contacts,groups,messages,updatedTo,m_settings.googleSheetIds());
    }
}

void CoreLogic::authError()
{
    qWarning() << "CORE: auth error";

    m_networkApiManager->endProcessing();
    m_uploadTimer.stop();
    m_accessTokenReceived=false;

    emit authProblem();
}

void CoreLogic::contactOrGroupAdded(QString skype_id, QString googleId,bool isContact)
{
    qInfo() << "contact or group chat is successfully added" << skype_id << googleId;

    m_settings.addSkypeUser(skype_id,"",googleId,isContact);
    m_settings.setLastSuccesfulRequestDate();

    emit refreshGUI();
    //m_settings.setGoogleSheetId(skype_id,googleId);

}

void CoreLogic::messagesAdded(QString skype_id, QDateTime uploadTo, QString lastregionA1)
{
    qInfo() << "messages is successfully uploaded" << skype_id << lastregionA1 << uploadTo;

    m_settings.setLastSync(skype_id,uploadTo,lastregionA1);
    m_settings.setLastSuccesfulRequestDate();

    emit refreshGUI();
}

void CoreLogic::folderWasCreated(QString name, QString id)
{
    qInfo() << "folder on google drive wa created id = " << id;

    m_settings.setGoogledrivefolder(name,id);
}


CoreThread::CoreThread(CoreLogic *logic, QObject *parent):QThread(parent),m_logic(logic)
{
    O_ASSERT(m_logic);
    m_logic->moveToThread(this);
    connect(this,SIGNAL(finished()),m_logic,SLOT(deleteLater()));
    start();
}

CoreThread::~CoreThread()
{
    qInfo() << "CORE: quit from core thread";
    quit();
    wait();
}





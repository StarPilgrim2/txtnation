#ifndef SYSTEMTRAY_H
#define SYSTEMTRAY_H

#include <QSystemTrayIcon>
#include <QMenu>

class SystemTray : public QSystemTrayIcon
{
    Q_OBJECT
public:    
    SystemTray(QObject *parent = 0);

public slots:
    void showMessage(QString title,QString message);

signals:
    void quitApp();

    void showGui();

    void showAbout();

private:
    QMenu m_menu;
};

#endif // SYSTEMTRAY_H

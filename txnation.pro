#-------------------------------------------------
#
# Project created by QtCreator 2016-08-22T11:05:39
#
#-------------------------------------------------

QT   += core gui network sql widgets


CONFIG += c++11

TARGET = txtnation
TEMPLATE = app

OBJECTS_DIR = .obj
MOC_DIR = .moc
UI_DIR = .ui
RCC_DIR = .rcc

LIBS += -lPsapi

win32 {
    RC_FILE = txtnation.rc
}

SOURCES += main.cpp\
        mainwindow.cpp \
    common.cpp \
    authgoogle.cpp \
    authhttpserver.cpp \
    spreadsheetmanager.cpp \
    corelogic.cpp \
    settings.cpp \
    skypereader.cpp \
    skypewatcher.cpp \
    networkmanager.cpp \
    spreadsheetqueue.cpp \
    skypeaccountsmodel.cpp \
    skypeaccountwidgets.cpp \
    systemtray.cpp \
    apllication.cpp \
    systeminfo.cpp
    #com/skype4comlib.cpp

HEADERS  += mainwindow.h \
    common.h \
    authgoogle.h \
    authhttpserver.h \
    spreadsheetmanager.h \
    corelogic.h \
    settings.h \
    skypereader.h \
    skypewatcher.h \
    networkmanager.h \
    spreadsheetqueue.h \
    skypeaccountsmodel.h \
    skypeaccountwidgets.h \
    systemtray.h \
    apllication.h \
    systeminfo.h
    #com/skype4comlib.h

FORMS    += \
    mainwindow.ui \
    skypeaccountwidgets.ui

RESOURCES += \
    txtnation.qrc

#ifndef OAUTHGOOGLE_H
#define OAUTHGOOGLE_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QTimer>
#include <qscopedpointer.h>
#include "authhttpserver.h"

class AuthHttpServer;
class QNetworkReply;

class AuthGoogle : public QObject
{
    Q_OBJECT
public:
    explicit AuthGoogle(QString refreshToken,QObject *parent = 0);

    void doAuth();


signals:
    void accessToken(QString token);
    void refreshToken(QString token);
    void authDenied();
    void authError();
protected:
    bool eventFilter(QObject *obj, QEvent *event);
private slots:

    void receivedAuthCode(QString code);
    void receiveToken();
    void refreshAcces();
    void parseToken(QByteArray data);
    void authErrorHandler();

private:

    void initRetryOnError();


    void buildTokenRequest(bool refresh,QString codeOrRefreshToken);

    static const QString m_clientID;
    static const QString m_clientSecret;
    static const QString m_authorizationEndpoint;
    static const QString m_tokenEndpoint;
    static const QString m_userInfoEndpoint;
    static const QString m_spreadsheetScope;
    static const QString m_driveScope;
    QScopedPointer<AuthHttpServer,QScopedPointerObjectDeleteLater<AuthHttpServer>> m_loopbackserver;
    QNetworkAccessManager m_networkManager;    
    //QScopedPointer<QNetworkReply,QScopedPointerObjectDeleteLater<QNetworkReply>> m_reply;
    QTimer m_refreshAccesTimer;
    QString m_refreshToken;
    QString m_loopbackUrl;
    static const int m_failbackinterval=30;
    QTimer m_retryTimer;
    QString m_code;
};

#endif // OAUTHGOOGLE_H

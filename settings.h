#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>
#include <QMap>
#include <QDateTime>


class Settings : public QSettings
{
public:
    Settings(QObject* parent=0);

    void addSkypeUser(QString skypeId,QString displayName,QString googleFileUid,bool isContact);
    QString displayName(QString skypeId) const;
    QStringList contactsSkypeIds();
    QStringList groupsSkypeIds();


    void setLastSync(QString skypeId,QDateTime date,QString lastInsertedregionA1note);
    QDateTime lastSync(QString skypeId) const;
    QMap<QString,QDateTime> lastSync();
    QString lastInsertedregion(QString skypeId) const;


    void setGoogleSheetId(QString skypeId,QString googleSheetId);
    QString googleSheetId(QString skypeId) const;
    QMap<QString,QString> googleSheetIds();

    void setUploadInterval(int secs);
    int uploadInterval() const;

    void setLastSuccesfulRequestDate(const QDateTime& date=QDateTime::currentDateTime());
    QDateTime lastSuccesfulRequestDate() const;

    void setInterestedMainDbs(QStringList pathes, QStringList names);
    QStringList interestedMainDbs() const;
    QStringList interestedMainDbsNames() const;

    bool firstStart() const;
    void setNonFirstStart();

    void setGoogledrivefolder(QString name,QString id);
    QString googleDriveFolder(QString name="Skype Backup") const;

    void setRefreshToken(QString token);
    QString refreshtoken() const;

private:
    static const int m_uploadDefault=300;//secs

};

#endif // SETTINGS_H

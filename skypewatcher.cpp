#include "skypewatcher.h"
#include <algorithm>
#include <QDebug>

SkypeWatcher::SkypeWatcher(QString file, QStringList contacts, QStringList groupChats, QMap<QString,QDateTime> lastmessageDate, QObject* parent):QObject(parent),m_reader(file),m_users(QSet<QString>::fromList(contacts)),m_chatGroups(QSet<QString>::fromList(groupChats)),m_lastmessageDate(lastmessageDate),m_timer(this)
{
    qInfo() << "Start sql watcher on " << file;    
    //m_watcher.addPath(file);

    m_timer.setInterval(m_readtimeout);
    connect(&m_timer,SIGNAL(timeout()),this,SLOT(mainDbUpdated()));
    m_timer.setSingleShot(false);
   // m_timer.start();

}

QMap<QString, QString> SkypeWatcher::newNormalContactList()
{
    return m_reader.normalContactList();
}

QMap<QString, QString> SkypeWatcher::newGroupchatList()
{
    return m_reader.groupchatList();
}

bool  SkypeWatcher::newMessages(QMap<QString, QList<QStringList>>& messages,QMap<QString,ListEditedMessage>& editedMessages,QMap<QString,ListDeletedMessage>& deletedMessages,
                                QMap<QString, QDateTime> &lastmessageDate, QMap<QString, QDateTime> &lastEditedMessageDate,QMap<QString, QDateTime> &lastDeltedMessageDate)
{
    messages = m_messages;
    lastmessageDate = m_lastmessageDate;
    m_messages.clear();

    editedMessages = m_EditedMessages;
    lastEditedMessageDate = m_lastEditedMessageDate;
    m_EditedMessages.clear();


    deletedMessages = m_DeletetedMessages;
    lastDeltedMessageDate = m_lastEditedMessageDate;
    m_DeletetedMessages.clear();



    return true;
}

 bool SkypeWatcher::allMessages(QMap<QString, QList<QStringList>>& messages,QMap<QString,ListEditedMessage>& editedMessages,QMap<QString,ListDeletedMessage>& deletedMessages,
                                QMap<QString, QDateTime> &lastmessageDate, QMap<QString, QDateTime> &lastEditedMessageDate,QMap<QString, QDateTime> &lastDeltedMessageDate)
{

     return retrieveMessages(messages,editedMessages,deletedMessages,lastmessageDate,lastEditedMessageDate,lastDeltedMessageDate);
 }

 void SkypeWatcher::startPeridicallyread()
 {
    m_timer.start();
     //connect(&m_watcher,SIGNAL(fileChanged(QString)),this,SLOT(mainDbUpdated()));

 }


void SkypeWatcher::mainDbUpdated()
{

    //qInfo() << "...";
    QMap<QString, QDateTime> temp;

    retrieveMessages(m_messages,m_EditedMessages,m_DeletetedMessages,temp,temp,temp);

}

bool SkypeWatcher::retrieveMessages(QMap<QString,QList<QStringList>>& messages, QMap<QString,ListEditedMessage> &editedMessages, QMap<QString,ListDeletedMessage> &deletedMessages, QMap<QString, QDateTime>& lastmessageDate, QMap<QString, QDateTime> &lastEditedMessageDate, QMap<QString, QDateTime> &lastDeltedMessageDate)
{
    //qDebug() << "retreive messages by dates" << m_lastmessageDate;

    QMap<QString,QList<QStringList>>  tempMessages;
    QMap<QString,ListEditedMessage> tempEditedMessages;
    QMap<QString,ListDeletedMessage> tempDeletedMessages;


    if (! m_reader.newMessages(m_lastmessageDate,tempMessages))
    {
        qDebug() << "SQL: can't normal retrive messages";
        return false;
    }

    if (! m_reader.newEditedMessages(m_lastmessageDate,tempEditedMessages))
    {
        qDebug() << "SQL: can't normal retrive edited messages";
        return false;
    }

    if (! m_reader.newDeletedMessages(m_lastmessageDate,tempDeletedMessages))
    {
        qDebug() << "SQL: can't normal retrive deleted messages";
        return false;
    }

    //qDebug() << "messages" << tempMessages;
    //qDebug() << "edited messages" << tempEditedMessages;
    //qDebug() << "deleted messages" << tempDeletedMessages;

    QMap<QString,QList<QStringList>>::iterator it =  tempMessages.begin();

    while (it != tempMessages.end())
    {
        QMap<QString,QList<QStringList>>::iterator it2 = messages.find(it.key());

        if (it2 != messages.end())
            it2.value().append(it.value());
        else
            messages[it.key()] = it.value();

        m_lastmessageDate[it.key()] = lastDate(it.value());

        it++;
    }

    lastmessageDate = m_lastmessageDate;


//    QMap<QString,ListEditedMessage>::iterator it3 =  tempEditedMessages.begin();

//    while (it3 != tempEditedMessages.end())
//    {
//        QMap<QString,ListEditedMessage>::iterator it4 = editedMessages.find(it.key());

//        if (it4 != editedMessages.end())
//            it4.value().append(it3.value());
//        else
//            editedMessages[it3.key()] = it3.value();

//        m_lastEditedMessageDate[it3.key()] = lastDate(it3.value());

//        it3++;
//    }

//    lastEditedMessageDate = m_lastEditedMessageDate;


//    QMap<QString,ListDeletedMessage>::iterator it5 =  tempDeletedMessages.begin();

//    while (it5 != tempDeletedMessages.end())
//    {
//        QMap<QString,ListDeletedMessage>::iterator it6 = deletedMessages.find(it.key());

//        if (it6 != deletedMessages.end())
//            it6.value().append(it5.value());
//        else
//            deletedMessages[it5.key()] = it5.value();

//        m_lastDeletedmessageDate[it6.key()] = lastDate(it5.value());

//        it5++;
//    }

//    lastDeltedMessageDate = m_lastDeletedmessageDate;



    return true;

}


QDateTime SkypeWatcher::lastDate(QList<QStringList> messages) const
{
    if (messages.isEmpty())
        return QDateTime();

    QStringList lst = messages.last();

    if (lst.count() != 3)
        return QDateTime();

    return QDateTime::fromString(lst.value(1));
}

QDateTime SkypeWatcher::lastDate(ListEditedMessage messages) const
{
    if (messages.isEmpty())
        return QDateTime();

    EditedMessage temp = messages.last();

    return temp.dateOfEdit;
}


QDateTime SkypeWatcher::lastDate(ListDeletedMessage messages) const
{
    if (messages.isEmpty())
        return QDateTime();

    DeletedMessage temp = messages.last();

    return temp.dateOfDeletion;
}

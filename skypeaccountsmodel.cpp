#include "skypeaccountsmodel.h"
#include "common.h"
#include <QFileInfo>

SkypeAccountsModel::SkypeAccountsModel(bool first, QObject *parent):QAbstractTableModel(parent),m_defaultChecedIndex(-1),m_first(first)
{
    //O_ASSERT(m_acounts.size() == m_displays.size() && m_displays.size() == m_paths.size());

    reloadData();
}

int SkypeAccountsModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_acounts.size();
}

int SkypeAccountsModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_columnCount;
}

QVariant SkypeAccountsModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role == Qt::DisplayRole)
    {
        if (index.column() == 0)
            return m_displays.value(index.row());

        if (index.column() == 1)
            return m_acounts.value(index.row());

        return QVariant();
    }

    if (role == Qt::CheckStateRole && index.column()==m_checkboxIndex)
    {
        return m_checks.value(index.row());
    }

    return QVariant();
}

Qt::ItemFlags SkypeAccountsModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags =  Qt::ItemIsEnabled;

    if (index.column() == m_checkboxIndex)
    {
        flags |= Qt::ItemIsUserCheckable;
    }

    return flags;

}

bool SkypeAccountsModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role == Qt::CheckStateRole && index.column() == m_checkboxIndex)
    {
      if (index.row() < m_checks.size())
      {
        m_checks[index.row()] = value.toInt();
        return true;
      }
    }
    return QAbstractTableModel::setData(index,value,role);
}

QVariant SkypeAccountsModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole || orientation != Qt::Horizontal)
        return QVariant();

    if (section == 0)
        return tr("Full name");
    if (section == 1)
        return tr("Skype name");

     return QVariant();

}

QStringList SkypeAccountsModel::interestedPaths() const
{
    QStringList temp;

    QStringList::const_iterator it1 = m_paths.begin();
    QList<int>::const_iterator it2 = m_checks.begin();

    while (it1 != m_paths.end())
    {
        if (*it2 == Qt::Checked)
            temp.push_back(*it1);

        it1++;
        it2++;
    }

    return temp;
}

QStringList SkypeAccountsModel::interestednames() const
{
    QStringList temp;

    QStringList::const_iterator it1 = m_displays.begin();
    QList<int>::const_iterator it2 = m_checks.begin();

    while (it1 != m_displays.end())
    {
        if (*it2 == Qt::Checked)
            temp.push_back(*it1);

        it1++;
        it2++;
    }

    return temp;
}

void SkypeAccountsModel::reloadData()
{
     beginResetModel();
     m_acounts.clear();
     m_displays.clear();
     m_paths.clear();
     m_checks.clear();
     m_defaultChecedIndex=-1;
     endResetModel();


     QPair<QStringList, QStringList> temp = locateSkypeMainDb();

     beginInsertRows(QModelIndex(),0,temp.second.count());

     m_paths = temp.second;

     QDateTime lastMod;

     int num=0;

     QSet<QString> pathes = QSet<QString>::fromList(m_settings.interestedMainDbs());

     for(QString path:m_paths)
     {
         SkypeReader reader(path);

         QPair<QString,QString> temp2 = reader.account();

         m_acounts.push_back(temp2.first);
         m_displays.push_back(temp2.second);

         if (m_first)
         {
            QFileInfo info(path);

            if (!lastMod.isValid() || info.lastModified() > lastMod)
            {
                lastMod = info.lastModified();
                m_defaultChecedIndex = num;
            }
            m_checks.push_back(Qt::Unchecked);
            num++;
         }
         else
         {
             m_checks.push_back(pathes.contains(path)?(Qt::Checked):Qt::Unchecked);
         }
     }

     if (m_first)
         m_checks[m_defaultChecedIndex] = Qt::Checked;

     endInsertRows();

}

void SkypeAccountsModel::save()
{
    m_settings.setInterestedMainDbs(interestedPaths(),interestednames());
}





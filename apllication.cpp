#include "apllication.h"
#include <QDebug>

Apllication::Apllication(int &argc, char *argv[], const QString uniqueKey):QApplication(argc,argv),m_uniqueKey(uniqueKey)
{
    sharedMemory.setKey(m_uniqueKey);
    if ( sharedMemory.attach() ) {
        m_isRunning = true;
    }
    else
    {
        m_isRunning = false;
        // create shared memory.
        if ( !sharedMemory.create(1) )
        {
            qCritical() << "Can't create shared memory";
            return;
        }
    }
}

bool Apllication::isRunning()
{
    return m_isRunning;
}

#ifndef APLLICATION_H
#define APLLICATION_H

#include <QObject>
#include <QApplication>
#include <QSharedMemory>

class Apllication : public QApplication
{
    Q_OBJECT
public:
        Apllication(int &argc, char *argv[], const QString uniqueKey);

        bool isRunning();
private:
        bool m_isRunning;
        QString m_uniqueKey;
        QSharedMemory sharedMemory;
};

#endif // APLLICATION_H

#include "common.h"
#include <QtGlobal>
#include <QProcessEnvironment>
#include <QDir>
#include <QStandardPaths>
#include <QMutex>
#include <QMutexLocker>
#include <QCoreApplication>


//qInfo noQuotesDebug() {
//    qInfo dbg = qInfo();
//    dbg.noquote();
//    return dbg;
//}

QString appDataPath()
{
//    QString appPath = QProcessEnvironment::systemEnvironment().value("APPDATA");
//    if (!appPath.endsWith(QDir::separator()))
//        appPath += QDir::separator();
//    return appPath;

    return QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).value(0) + QDir::separator();
}

QString skypeAppDataPath()
{
    QDir appDir = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).value(0);

    appDir.cdUp();

    return appDir.path() + QDir::separator() + APP_SKYPE_NAME;
}

QPair<QStringList, QStringList> locateSkypeMainDb()
{
    QPair<QStringList, QStringList> res;

    QDir root = skypeAppDataPath();

    for(QFileInfo info:root.entryInfoList(QDir::Dirs|QDir::NoDotAndDotDot|QDir::NoSymLinks))
    {
        QString maindbPath = info.absoluteFilePath() + "/main.db";
        if (QFile::exists(maindbPath))
        {
            res.first.append(info.fileName());
            res.second.append(maindbPath);
        }
    }
    return res;
}

static QFile logFile;
static QTextStream logStream;
static QDate toDay;
static bool bIsTerminating = false;

#ifdef QT_DEBUG
    static QString logformat("[%{time HH:mm:ss}] %{if-debug}[DEBUG]%{endif}%{if-warning}[WARNING]%{endif}%{if-critical}[CRITICAL]%{endif}%{if-fatal}[FATAL]%{endif} %{file}:%{line} - %{message}\n");
#else
    static QString logformat("[%{time HH:mm:ss}] %{if-debug}[DEBUG]%{endif}%{if-warning}[WARNING]%{endif}%{if-critical}[CRITICAL]%{endif}%{if-fatal}[FATAL]%{endif} %{message}\n");
#endif


static void reInitLog();
static void writeLog(const QString & text);

static void logMessageOutput(QtMsgType type,const QMessageLogContext& context,const QString& msg)
{

  static QMutex mutex(QMutex::Recursive);
  QMutexLocker locker(&mutex);

    if (bIsTerminating) return;
    if(!logFile.isOpen()) return;


  QDateTime tm = QDateTime::currentDateTime();

  if ( tm.date() != toDay )
    reInitLog();

  logStream << qFormatLogMessage(type,context,msg);

  logStream.flush();
}

static void cleanupMonitor()
{
  bIsTerminating = true;
  writeLog(QString("[" + QDateTime::currentDateTime().toString("HH:mm:ss") + "] End of work"));
  logStream.flush();
  logFile.flush();
  logFile.close();
}

static void reInitLog()
{
  logStream.flush();
  logFile.flush();
  logFile.close();
  initLog();
}


void initLog(){

  qSetMessagePattern(logformat);
  bIsTerminating = false;

  QDir logDir(logsPath());

  if (!logDir.exists())
      logDir.mkpath(logsPath());

  QString logPath = logsPath()+QDate::currentDate().toString("yyyyMMdd")+".log";
  toDay = QDate::currentDate();
  logFile.setFileName(logPath);
  logFile.open(QIODevice::WriteOnly |  QIODevice::Append);
  logStream.setDevice(&logFile);
  qInstallMessageHandler(logMessageOutput);
  qAddPostRoutine(cleanupMonitor);
}

void writeLog(const QString & text){
  logStream << text << "\n";
}


QString logsPath()
{
    return appDataPath() + "/logs/";
}

int defaultNetworkInterval()
{
    return 30;
}

QStringList Message::toList() const
{
    return (QStringList() << From << Date.toString() << Text);
}

QDateTime Message::KeyDate() const
{
    return Date;
}

QStringList EditedMessage::toList() const
{
    return Message::toList() << newText << dateOfEdit.toString();
}

QDateTime EditedMessage::KeyDate() const
{
    return dateOfEdit;
}

QStringList DeletedMessage::toList() const
{
    return Message::toList()  << dateOfDeletion.toString();
}

QDateTime DeletedMessage::KeyDate() const
{
    return dateOfDeletion;
}

QString appVersion()
{
    return "v. " + QString::number(VERSION_MAJOR) + "." + QString::number(VERSION_MINOR) + "." + QString::number(BUILD_NUMBER);
}

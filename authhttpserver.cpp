#include "common.h"
#include "authhttpserver.h"

#include <QTcpServer>
#include <QTcpSocket>
#include <Qurl>
#include <QUrlQuery>
#include <QFile>


AuthHttpServer::AuthHttpServer(QObject* parent) : QObject(parent),m_server(this)
{
    connect(&m_server,SIGNAL(newConnection()),this,SLOT(newConnection()));
}

AuthHttpServer::~AuthHttpServer()
{

}

QString AuthHttpServer::loopbackUrl() const
{
    QUrl loopbackUrl;

    QHostAddress loopbackAddr(QHostAddress::LocalHost);
    loopbackUrl.setScheme("http");
    loopbackUrl.setHost(loopbackAddr.toString());
    loopbackUrl.setPort(m_server.serverPort());

    return loopbackUrl.toString();
}

bool AuthHttpServer::listen()
{
    if (m_server.isListening())
        return true;

    bool res =  m_server.listen();

    if (!res)
        qCritical() << "AUTH: server is not listening";
    else
        qInfo() << "AUTH: server is listening";

    return res;
}



void AuthHttpServer::newConnection()
{
    /*if (m_socket)
        return;
    O_ASSERT(m_socket.isNull());*/

    qInfo() << "AUTH: new incoming connection";

    //m_responseBuffer.clear();

    //if (m_socket)
    //    m_socket->disconnect();

    //m_socket.reset(m_server.nextPendingConnection());

    QTcpSocket* socket = m_server.nextPendingConnection();

    m_responseBuffer[socket] = QByteArray();

    if (!socket)
    {
        qWarning() << "AUTH: failed to get pending connection";
        return;
    }

    connect(socket,SIGNAL(readyRead()),this,SLOT(readData()));
    connect(socket,SIGNAL(aboutToClose()),this,SLOT(closeConnection()));
    connect(socket,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(errorHandler(QAbstractSocket::SocketError)));
    //connect(m_socket.data(),SIGNAL(stateChanged(QAbstractSocket::SocketState)),this,SLOT(stateChanged(QAbstractSocket::SocketState)));

    //processing();

    //stop();

    //qInfo() << "AUTH: server is stopped";
}

void AuthHttpServer::readData()
{
    QTcpSocket* socket = dynamic_cast<QTcpSocket*>(sender());

    if (!socket)
    {
        qCritical() << "AUTH: failed read from socket";
        return;
    }

    if (!m_responseBuffer.contains(socket))
    {
        qCritical() << "AUTH: failed read from unregistred socket";
        return;
    }

    QByteArray buff = m_responseBuffer[socket];

    buff.append(socket->readAll());

    if (buff.contains("\r\n\r\n") || buff.contains("\n\n"))
    {
        processing(buff,socket);
    }
}

void AuthHttpServer::closeConnection()
{
    QTcpSocket* socket = dynamic_cast<QTcpSocket*>(sender());

    if (!socket)
    {
        qCritical() << "AUTH: failed read from socket";
        return;
    }

    if (!m_responseBuffer.contains(socket))
    {
        qCritical() << "AUTH: failed read from unregistred socket";
        return;
    }

    QByteArray buff = m_responseBuffer[socket];

    buff.append(socket->readAll());

    processing(buff,socket);

}

void AuthHttpServer::errorHandler(QAbstractSocket::SocketError err)
{
    QTcpSocket* socket = dynamic_cast<QTcpSocket*>(sender());

    if (!socket)
    {
        qCritical() << "AUTH: failed read from socket";
        return;
    }

    qCritical() << "AUTH: socket error"  << err << socket->errorString();


    if (!m_responseBuffer.contains(socket))
    {
        qCritical() << "AUTH: failed read from unregistred socket";
        return;
    }

    QByteArray buff = m_responseBuffer[socket];

    buff.append(socket->readAll());

    processing(buff,socket);

}


void AuthHttpServer::processing(QByteArray& responseBuffer , QTcpSocket *socket)
{
    qDebug() << "AUTH: request" << responseBuffer;
    socket->disconnect();


    responseBuffer.replace("\r\n","\n");

    QList<QByteArray> headerList = responseBuffer.split('\n');

    qDebug() << headerList;

    QString reqStr = headerList.value(0);

    qDebug() << reqStr;

    QUrl tempUrl(reqStr.split(" ").value(1));

    QUrlQuery tempQuery(tempUrl.query());

    bool access_denied = !tempQuery.queryItemValue("error").isEmpty();

    QString aCode = tempQuery.queryItemValue("code");

    QString response;

    if (access_denied)
    {
        qInfo() << "AUTH: authorization is denied";

        emit authDenied();


        //QFile file(":/gui/html/refuse.html");
        //file.open(QFile::ReadOnly);

        //response = prepareResponse(file.readAll(),true);

        response = prepareRedirectTo("https://clients.txtnation.com/hc/en-us/articles/227042968#authorization");
    }
    else if (!aCode.isEmpty())
    {
        qInfo() << "AUTH: authorization is accepted";

        //QFile file(":/gui/html/accept.html");
        emit authCode(aCode);

        //file.open(QFile::ReadOnly);

        response = prepareRedirectTo("https://clients.txtnation.com/hc/en-us/articles/227042968#congratulations");
        //response = prepareResponse(file.readAll(),true);

        for (QTcpSocket* currSocket: m_server.findChildren<QTcpSocket*>())
        {
            if (currSocket && currSocket != socket)
            {
                currSocket->disconnect();
                currSocket->close();
                socket->deleteLater();
            }
        }
        m_server.close();
    }
    //no auth response!!!!!!
    else
    {
        response = prepareResponse(QByteArray(),false);
    }

    qDebug() << response;

    socket->write(response.toLatin1());
    socket->flush();

    socket->close();

    socket->deleteLater();
}


QString AuthHttpServer::prepareResponse(QString body, bool httpOk) const
{

    QString res;

    if (httpOk)
        res = "HTTP/1.1 200 OK\r\n";
    else
        res = "HTTP/1.1 404 Not Found\r\n";

    res += "Content-Length: " + QString::number(body.size()) + "\r\n";
    res += "Content-Type: text/html; charset=\"utf-8\"\r\n";
    res += "Connection: close\r\n\r\n";

    res += body;

    return res;
}

QString AuthHttpServer::prepareRedirectTo(QString location) const
{
    return "HTTP/1.1 301 Moved Permanently\r\nLocation: " + location + "\r\n\r\n";
}




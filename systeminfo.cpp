#include "systeminfo.h"
#include <QSharedData>
#include <QSettings>
#include <QString>
#include <QList>
#include <QApplication>
#include <QDesktopWidget>
#include <QNetworkConfigurationManager>
#include <QDateTime>
#include <QDebug>

#ifdef Q_OS_WIN
#define WINVER 0x500
#include <windows.h>
#include <winnt.h>
#include <winbase.h>
#include <cpuid.h>
#include <Psapi.h>
#define _UNICODE
#include <tchar.h>
#include <strsafe.h>
#include <string.h>
#include <wchar.h>
#define BUFSIZE 256
typedef void (WINAPI *PGNSI)(LPSYSTEM_INFO);
typedef BOOL (WINAPI *PGPI)(DWORD, DWORD, DWORD, DWORD, PDWORD);
//#define PRODUCT_UNDEFINED                       0x00000000

//#define PRODUCT_ULTIMATE                        0x00000001
//#define PRODUCT_HOME_BASIC                      0x00000002
//#define PRODUCT_HOME_PREMIUM                    0x00000003
//#define PRODUCT_ENTERPRISE                      0x00000004
//#define PRODUCT_HOME_BASIC_N                    0x00000005
//#define PRODUCT_BUSINESS                        0x00000006
//#define PRODUCT_STANDARD_SERVER                 0x00000007
//#define PRODUCT_DATACENTER_SERVER               0x00000008
//#define PRODUCT_SMALLBUSINESS_SERVER            0x00000009
//#define PRODUCT_ENTERPRISE_SERVER               0x0000000A
//#define PRODUCT_STARTER                         0x0000000B
//#define PRODUCT_DATACENTER_SERVER_CORE          0x0000000C
//#define PRODUCT_STANDARD_SERVER_CORE            0x0000000D
//#define PRODUCT_ENTERPRISE_SERVER_CORE          0x0000000E
//#define PRODUCT_ENTERPRISE_SERVER_IA64          0x0000000F
//#define PRODUCT_BUSINESS_N                      0x00000010
//#define PRODUCT_WEB_SERVER                      0x00000011
//#define PRODUCT_CLUSTER_SERVER                  0x00000012
//#define PRODUCT_HOME_SERVER                     0x00000013
//#define PRODUCT_STORAGE_EXPRESS_SERVER          0x00000014
//#define PRODUCT_STORAGE_STANDARD_SERVER         0x00000015
//#define PRODUCT_STORAGE_WORKGROUP_SERVER        0x00000016
//#define PRODUCT_STORAGE_ENTERPRISE_SERVER       0x00000017
//#define PRODUCT_SERVER_FOR_SMALLBUSINESS        0x00000018
//#define PRODUCT_SMALLBUSINESS_SERVER_PREMIUM    0x00000019

//#define PRODUCT_UNLICENSED                      0xABCDABCD
#endif

class SystemInfoData : public QSharedData {
public:
    QString m_osVersion;
    QList<QRect> m_resolutions;
    int m_primaryScreen;
    QString m_processorBrand;
    QString m_memoryInfo;
    QStringList m_interfacesList;
    QString m_activeInterface;

    SystemInfo::WinVers m_winVers;
    QString m_frequency; //������� ����� � ����������
    QString m_motherBoardManufacturerId; //id ������������� ��� �����, ��� ��� ���� ������� ������ � ������
    qulonglong m_startTime;
};

SystemInfo::SystemInfo() : data(new SystemInfoData)
{
    //qDebug() << "SystemInfo1";
    data->m_osVersion    = getOsVersion(data->m_winVers);
    //qDebug() << "SystemInfo2";
    QDesktopWidget* desk =  QApplication::desktop();

    //qDebug() << "SystemInfo3";
    if (desk)
    {
        int screenCount = desk->screenCount();
        int i=0;
        while (i < screenCount)
        {
            data->m_resolutions.push_back(desk->screenGeometry(i));
            i++;
        }
        data->m_primaryScreen = desk->primaryScreen();
    }

    //qDebug() << "SystemInfo4";

    data->m_processorBrand = getProcessorBrand();

    //qDebug() << "SystemInfo5";

    data->m_memoryInfo = getMemoryInfo();

    //qDebug() << "SystemInfo6";

    getInterfaces(data->m_interfacesList,data->m_activeInterface);

    //qDebug() << "SystemInfo7";

    data->m_frequency = getFrequencyCpuMhz();

    data->m_motherBoardManufacturerId=getMotherBoardManufacturerId();

    data->m_startTime = QDateTime::currentDateTime().toTime_t();

}

const QString& SystemInfo::OsVersion() const
{
   return data->m_osVersion;
}

const QList<QRect>& SystemInfo::resolutions() const
{
    return data->m_resolutions;
}

int SystemInfo::primaryScreen() const
{
    return data->m_primaryScreen;
}

const QString& SystemInfo::processorBrand() const
{
    return data->m_processorBrand;
}

const QString& SystemInfo::memoryInfo() const
{
   return data->m_memoryInfo;
}

const QStringList& SystemInfo::networkInterfaces() const
{
    return data->m_interfacesList;
}

const QString& SystemInfo::activeInterface() const
{
    return data->m_activeInterface;
}

SystemInfo::WinVers SystemInfo::winVersion() const
{
    return data->m_winVers;
}

QString SystemInfo::cpuFrequencyMhz() const
{
    return data->m_frequency;
}

QString SystemInfo::motherBoardManufacturerId() const
{
    return data->m_motherBoardManufacturerId;
}

qulonglong SystemInfo::progUpTime() const
{
  qlonglong val = QDateTime::currentDateTime().toTime_t() - data->m_startTime;

  if (val <0)
      return 0;

  return val;
}

qulonglong SystemInfo::allMemoryBytes() const
{
    MEMORYSTATUSEX info;
    info.dwLength = sizeof(info);
    if (!GlobalMemoryStatusEx(&info))
    {
        return 0;
    }
    return info.ullTotalPhys;
}

qulonglong SystemInfo::availMemoryBytes() const
{
    MEMORYSTATUSEX info;
    info.dwLength = sizeof(info);
    if (!GlobalMemoryStatusEx(&info))
    {
        return 0;
    }
    return info.ullAvailPhys;
}

qulonglong SystemInfo::selfApppMemoryBytes() const
{
    PROCESS_MEMORY_COUNTERS memCounter;
    bool result = GetProcessMemoryInfo(GetCurrentProcess(),&memCounter,sizeof( memCounter ));

    if (!result)
        return 0;

    return memCounter.WorkingSetSize;
}

qulonglong SystemInfo::systemUpTimeSecs() const
{
    DWORD bootTime =  GetTickCount();
    return bootTime/1000;
}

QString SystemInfo::getOsVersion(WinVers &vers)
{
#ifdef Q_OS_WIN
    vers = Unknown;
    //��� ���� �� ��������������� ������� � ����������� ��� MinGW
    TCHAR pszOS[BUFSIZE]=L"";
    OSVERSIONINFOEX osvi;
    SYSTEM_INFO si;
    PGNSI pGNSI;
    PGPI pGPI;
    BOOL bOsVersionInfoEx;
    DWORD dwType;

    ZeroMemory(&si, sizeof(SYSTEM_INFO));
    ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));

    osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
    bOsVersionInfoEx = GetVersionEx((OSVERSIONINFO*) &osvi);

    if(bOsVersionInfoEx != TRUE ) return "Unknown Windows System";

    // Call GetNativeSystemInfo if supported or GetSystemInfo otherwise.

    //����� ������� ������������ ��� ������ ������������� - �� �����-�� ������� ����� ������ ��� ���  �������
    pGNSI = (PGNSI) GetProcAddress(
                GetModuleHandle(_TEXT("kernel32.dll")),
                "GetNativeSystemInfo");
    if(NULL != pGNSI)
        pGNSI(&si);
    else GetSystemInfo(&si);

    //ODebug() << osvi.dwMajorVersion;

    if ( VER_PLATFORM_WIN32_NT==osvi.dwPlatformId &&
         osvi.dwMajorVersion > 4 )
    {
        StringCchCat(pszOS,BUFSIZE,_TEXT("Microsoft "));

        // Test for the specific product.

        if ( osvi.dwMajorVersion == 10 )
        {
            if( osvi.dwMinorVersion == 0 )
            {
                if( osvi.wProductType == VER_NT_WORKSTATION )
                    StringCchCat(pszOS,BUFSIZE,_TEXT("Windows 10 Insider Preview "));
                else StringCchCat(pszOS,BUFSIZE,_TEXT("Windows Server Technical Preview " ));
            }

        }

        if ( osvi.dwMajorVersion == 6 )
        {
            if( osvi.dwMinorVersion == 0 )
            {
                if( osvi.wProductType == VER_NT_WORKSTATION )
                    StringCchCat(pszOS,BUFSIZE,_TEXT("Windows Vista "));
                else StringCchCat(pszOS,BUFSIZE,_TEXT("Windows Server 2008 " ));
            }

            if ( osvi.dwMinorVersion == 1 )
            {
                if( osvi.wProductType == VER_NT_WORKSTATION )
                {
                    StringCchCat(pszOS,BUFSIZE,_TEXT("Windows 7 "));
                    vers = Win_7;
                }
                else StringCchCat(pszOS,BUFSIZE,_TEXT("Windows Server 2008 R2 " ));
            }

            if ( osvi.dwMinorVersion == 2 )
            {
                if( osvi.wProductType == VER_NT_WORKSTATION )
                {
                    StringCchCat(pszOS,BUFSIZE,_TEXT("Windows 8 "));
                    vers = Win_7;
                }
                else StringCchCat(pszOS,BUFSIZE,_TEXT("Windows Server 2012 " ));
            }

            if ( osvi.dwMinorVersion == 3 )
            {
                if( osvi.wProductType == VER_NT_WORKSTATION )
                {
                    StringCchCat(pszOS,BUFSIZE,_TEXT("Windows 8.1 "));
                    vers = Win_7;
                }
                else StringCchCat(pszOS,BUFSIZE,_TEXT("Windows Server 2012 R2 " ));
            }

            pGPI = (PGPI) GetProcAddress(
                        GetModuleHandle(_TEXT("kernel32.dll")),
                        "GetProductInfo");

            pGPI( osvi.dwMajorVersion, osvi.dwMinorVersion, 0, 0, &dwType);

            switch( dwType )
            {
            case PRODUCT_ULTIMATE:
                StringCchCat(pszOS,BUFSIZE,_TEXT("Ultimate Edition" ));
                break;
            case PRODUCT_HOME_BASIC:
                StringCchCat(pszOS,BUFSIZE,_TEXT("Home Basic Edition" ));
                break;
            case PRODUCT_HOME_PREMIUM:
                StringCchCat(pszOS,BUFSIZE,_TEXT("Home Premium Edition" ));
                break;
            case PRODUCT_ENTERPRISE:
                StringCchCat(pszOS,BUFSIZE,_TEXT("Enterprise Edition" ));
                break;
            case PRODUCT_BUSINESS:
                StringCchCat(pszOS,BUFSIZE,_TEXT("Business Edition" ));
                break;
            case PRODUCT_STARTER:
                StringCchCat(pszOS,BUFSIZE,_TEXT("Starter Edition" ));
                break;
            case PRODUCT_CLUSTER_SERVER:
                StringCchCat(pszOS,BUFSIZE,_TEXT("Cluster Server Edition" ));
                break;
            case PRODUCT_DATACENTER_SERVER:
                StringCchCat(pszOS,BUFSIZE,_TEXT("Datacenter Edition" ));
                break;
            case PRODUCT_DATACENTER_SERVER_CORE:
                StringCchCat(pszOS,BUFSIZE,_TEXT("Datacenter Edition (core installation)" ));
                break;
            case PRODUCT_ENTERPRISE_SERVER:
                StringCchCat(pszOS,BUFSIZE,_TEXT("Enterprise Edition" ));
                break;
            case PRODUCT_ENTERPRISE_SERVER_CORE:
                StringCchCat(pszOS,BUFSIZE,_TEXT("Enterprise Edition (core installation)" ));
                break;
            case PRODUCT_ENTERPRISE_SERVER_IA64:
                StringCchCat(pszOS,BUFSIZE,_TEXT("Enterprise Edition for Itanium-based Systems" ));
                break;
            case PRODUCT_SMALLBUSINESS_SERVER:
                StringCchCat(pszOS,BUFSIZE,_TEXT("Small Business Server" ));
                break;
            case PRODUCT_SMALLBUSINESS_SERVER_PREMIUM:
                StringCchCat(pszOS,BUFSIZE,_TEXT("Small Business Server Premium Edition" ));
                break;
            case PRODUCT_STANDARD_SERVER:
                StringCchCat(pszOS,BUFSIZE,_TEXT("Standard Edition" ));
                break;
            case PRODUCT_STANDARD_SERVER_CORE:
                StringCchCat(pszOS,BUFSIZE,_TEXT("Standard Edition (core installation)" ));
                break;
            case PRODUCT_WEB_SERVER:
                StringCchCat(pszOS,BUFSIZE,_TEXT("Web Server Edition" ));
                break;
            }
        }

        if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 2 )
        {
            if( GetSystemMetrics(SM_SERVERR2) )
                StringCchCat(pszOS,BUFSIZE,_TEXT( "Windows Server 2003 R2, "));
            else if ( osvi.wSuiteMask & VER_SUITE_STORAGE_SERVER )
                StringCchCat(pszOS,BUFSIZE,_TEXT( "Windows Storage Server 2003"));
            else if( osvi.wProductType == VER_NT_WORKSTATION &&
                     si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_AMD64)
            {
                StringCchCat(pszOS,BUFSIZE,_TEXT( "Windows XP Professional x64 Edition"));
            }
            else StringCchCat(pszOS,BUFSIZE,_TEXT("Windows Server 2003, "));

            // Test for the server type.
            if ( osvi.wProductType != VER_NT_WORKSTATION )
            {
                if ( si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_IA64 )
                {
                    if( osvi.wSuiteMask & VER_SUITE_DATACENTER )
                        StringCchCat(pszOS,BUFSIZE,_TEXT( "Datacenter Edition for Itanium-based Systems" ));
                    else if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
                        StringCchCat(pszOS,BUFSIZE,_TEXT( "Enterprise Edition for Itanium-based Systems" ));
                }

                else if ( si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_AMD64 )
                {
                    if( osvi.wSuiteMask & VER_SUITE_DATACENTER )
                        StringCchCat(pszOS,BUFSIZE,_TEXT( "Datacenter x64 Edition" ));
                    else if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
                        StringCchCat(pszOS,BUFSIZE,_TEXT( "Enterprise x64 Edition" ));
                    else StringCchCat(pszOS,BUFSIZE,_TEXT( "Standard x64 Edition" ));
                }

                else
                {
                    if ( osvi.wSuiteMask & VER_SUITE_COMPUTE_SERVER )
                        StringCchCat(pszOS,BUFSIZE,_TEXT( "Compute Cluster Edition" ));
                    else if( osvi.wSuiteMask & VER_SUITE_DATACENTER )
                        StringCchCat(pszOS,BUFSIZE,_TEXT( "Datacenter Edition" ));
                    else if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
                        StringCchCat(pszOS,BUFSIZE,_TEXT( "Enterprise Edition" ));
                    else if ( osvi.wSuiteMask & VER_SUITE_BLADE )
                        StringCchCat(pszOS,BUFSIZE,_TEXT( "Web Edition" ));
                    else StringCchCat(pszOS,BUFSIZE,_TEXT( "Standard Edition" ));
                }
            }
        }

        if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 1 )
        {
            StringCchCat(pszOS,BUFSIZE,_TEXT("Windows XP "));
            if( osvi.wSuiteMask & VER_SUITE_PERSONAL )
                StringCchCat(pszOS,BUFSIZE,_TEXT( "Home Edition" ));
            else StringCchCat(pszOS,BUFSIZE,_TEXT( "Professional" ));
        }

        if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 0 )
        {
            StringCchCat(pszOS,BUFSIZE,_TEXT("Windows 2000 "));

            if ( osvi.wProductType == VER_NT_WORKSTATION )
            {
                StringCchCat(pszOS,BUFSIZE,_TEXT( "Professional" ));
            }
            else
            {
                if( osvi.wSuiteMask & VER_SUITE_DATACENTER )
                    StringCchCat(pszOS,BUFSIZE,_TEXT( "Datacenter Server" ));
                else if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
                    StringCchCat(pszOS,BUFSIZE,_TEXT( "Advanced Server" ));
                else StringCchCat(pszOS,BUFSIZE,_TEXT( "Server" ));
            }
        }

        // Include service pack (if any) and build number.

        size_t len=0;

        StringCchLength(osvi.szCSDVersion,128,&len);

        if( len > 0 )
        {
            StringCchCat(pszOS,BUFSIZE,_TEXT(" ") );
            StringCchCat(pszOS,BUFSIZE,osvi.szCSDVersion);
        }

        static const int buffSize=80;
        TCHAR buf[buffSize];

        StringCchPrintf(buf,buffSize, _TEXT(" (build %d)"), osvi.dwBuildNumber);
        StringCchCat(pszOS,BUFSIZE,buf);

        if ( osvi.dwMajorVersion >= 6 )
        {
            if ( si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_AMD64 )
                StringCchCat(pszOS,BUFSIZE,_TEXT( ", 64-bit" ));
            else if (si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_INTEL )
                StringCchCat(pszOS,BUFSIZE,_TEXT(", 32-bit"));
        }

        return QString::fromWCharArray(pszOS).trimmed();
    }

    else
    {
        return "Unknown Windows system!";
    }
#endif
    return "Not Windows system";
}

QString SystemInfo::getProcessorBrand()
{
#ifdef Q_OS_WIN
    QString res;
    uint CPUInfo[4] = {static_cast<uint>(-1)};
    __cpuid(0x80000000,CPUInfo[0],CPUInfo[1],CPUInfo[2],CPUInfo[3]);
    //��� ��������� ���� ������� ����� ������ �������� �������� ���� __cpuid  - ���� � ������ ������ ������������ ������ �������� � ��������� - ������ ��� ���� � ����
    uint totalIds = CPUInfo[0];

    //��� ����������� �������������� SSE3 - �.�. ��� ����������� ����������� ������� �����������
    if (0x80000004 <= totalIds )
    {
        __cpuid(0x80000002,CPUInfo[0],CPUInfo[1],CPUInfo[2],CPUInfo[3]);
        res += QString::fromLatin1(reinterpret_cast<const char*>(CPUInfo),sizeof(CPUInfo)).trimmed();
        __cpuid(0x80000003,CPUInfo[0],CPUInfo[1],CPUInfo[2],CPUInfo[3]);
        res += QString::fromLatin1(reinterpret_cast<const char*>(CPUInfo),sizeof(CPUInfo)).trimmed();
        __cpuid(0x80000004,CPUInfo[0],CPUInfo[1],CPUInfo[2],CPUInfo[3]);
        res += QString::fromLatin1(reinterpret_cast<const char*>(CPUInfo),sizeof(CPUInfo)).trimmed();

        res = res.replace(QRegExp(" +")," ");
        res = res.replace('\n',"");
        res = res.replace('\t',"");
        res = res.replace('\r',"");
        res = res.replace(QChar('\0'),"");
    }
    else
      res="Unknown processor not supported SSE3";
    return res;

#endif
    return "";
}

QString SystemInfo::getMemoryInfo()
{
#ifdef Q_OS_WIN
    MEMORYSTATUSEX info;
    info.dwLength = sizeof(info);
    if (!GlobalMemoryStatusEx(&info))
    {
        //DWORD res = GetLastError();
        return "Unknow size of physical memory";
    }
    QString res;

    uint gbCount = info.ullTotalPhys/(1024*1024*1024);
    uint mbCount = (info.ullTotalPhys%(1024*1024*1024))/(1024*1024);

    if (gbCount)
        res += QString::number(gbCount) + " Gb ";
    if (mbCount)
        res += QString::number(mbCount) + " Mb ";
    return res;

#endif
    return "";
}

void SystemInfo::getInterfaces(QStringList& interfaces,QString& active)
{
    QNetworkConfigurationManager manager;

    QList<QNetworkConfiguration> confs = manager.allConfigurations();
    interfaces.clear();
    active="";

    foreach(QNetworkConfiguration conf,confs)
    {
        if (!conf.isValid() || conf.bearerType() == QNetworkConfiguration::BearerUnknown)
            continue;
       QString row = conf.bearerTypeName()+ " " + conf.name();
       row += networStateString(conf);
       row += roamingStr(conf);
       interfaces.push_back(row);
    }


    QNetworkConfiguration defConf = manager.defaultConfiguration();
    if (!defConf.isValid() || defConf.bearerType() == QNetworkConfiguration::BearerUnknown)
        return;
    active = defConf.bearerTypeName() + " " + defConf.name();
    active += networStateString(defConf);
    active += roamingStr(defConf);


}

QString SystemInfo::roamingStr(const QNetworkConfiguration& conf) const
{
    QString res;
    if (conf.bearerType() == QNetworkConfiguration::Bearer2G || conf.bearerType() == QNetworkConfiguration::BearerCDMA2000 ||
               conf.bearerType() == QNetworkConfiguration::BearerWCDMA || conf.bearerType() == QNetworkConfiguration::BearerHSPA)
               res += (QString(" roaming: ") + (conf.isRoamingAvailable()?"yes":"no"));
    return res;
}

QString SystemInfo::networStateString(const QNetworkConfiguration& conf) const
{
    QNetworkConfiguration::StateFlags flgs = conf.state();
    QString res = " status: ";


    if (flgs.testFlag(QNetworkConfiguration::Active))
    {
        res += "active";
        //������ ��� ���� �������� �� � Defined � Discovered � �� Undefined
    }
    else if (flgs.testFlag(QNetworkConfiguration::Discovered))
    {
        res += "discovered";
        //���� Discovered �� � Defined � �� Undefined
    }
    else if (flgs.testFlag(QNetworkConfiguration::Defined))
    {
        res += "defined";
        //���� Defined �� �� Undefined
    }

    else if (flgs.testFlag(QNetworkConfiguration::Undefined))
    {
        res += "undefined";
        //���� ��� - �� ��������� ������ ������
    }
    else
    {
        res += "unknown state";
    }

    return res;

}

QString SystemInfo::getFrequencyCpuMhz() const
{
    QSettings settings("HKEY_LOCAL_MACHINE\\HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0",QSettings::NativeFormat);
    QVariant ret = settings.value("~MHz");
    bool ok=false;
    quint32 res = ret.toUInt(&ok);
    if (!ok)
        return QString();
    return QString::number(res);
}

QString SystemInfo::getMotherBoardManufacturerId() const
{
    //����� ������ �����, ��������������� � ���� ������!!!������ � ��� ��� �������� ������� ������
    QSettings settings("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Enum\\PCI",QSettings::NativeFormat);
//ODebug() << settings.childGroups() << settings.childGroups().count();
    static const QString pattern="SUBSYS_";
    static const auto exceptSet=QSet<QString>::fromList(QList<QString>() << "9710" << "1000" << "1002" << "10EC");
    QString res;

    foreach (QString key, settings.childGroups())
    {
        //ODebug() << key;

        int index=key.indexOf(pattern);

        if (index==-1)
            continue;

        index += pattern.length();

        QString temp=key.mid(index,8);

        //� ���� ��� ������ ��� ��� ���������...���� ���� ���� � ������
        temp=temp.right(4);

        //ODebug() << temp;

        if (exceptSet.contains(temp))
                continue;

        res = temp;
    }

    return res;
}


const SystemInfo &SystemInfo::instance()
{
    static SystemInfo info = SystemInfo();
    return info;
}

SystemInfo::~SystemInfo()
{
}

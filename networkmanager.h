#ifndef NETWORKMANAGER_H
#define NETWORKMANAGER_H

#include <QObject>
#include <QDateTime>
#include <QMap>
#include <QTimer>
#include <QNetworkAccessManager>
#include <QSharedPointer>
#include "spreadsheetmanager.h"

class SpreadsheetQueue;


class NetworkManager : public QObject
{
    Q_OBJECT
public:
    explicit NetworkManager( QString googleFolderId, QObject *parent = 0);

    void addData(QMap<QString,QString> constacts, QMap<QString,QString> groupsChats, QMap<QString,QList<QStringList>> messages, QMap<QString, QDateTime> updatedTo,QMap<QString,QString> googleSheetsIds);

    void startProcessing();

    bool endProcessing();

signals:

    void contactOrGroupAdded(QString skype_id,QString googleId,bool isContact);

    void messagesAdded(QString skype_id,QDateTime uploadTo,QString lastregionA1);

    void folderWasCreated(QString name,QString id);

public slots:

    void folderIsExist(QString name,QString id,bool exist);

    void folderCreated(QString name,QString id);

    void setAcessToken(QString token);

    void processNetwork();

private slots:
    void errorHandler(SpreadsheetManager::ErrorType errType, SpreadsheetManager::RequestType reqType, QMap<QString, QString> metaParam, QString errorText);

private:

    void checkFolder();

    void timerRestartExponental();

    void timerRestartInital();

    SpreadsheetQueue* createQueue(QString skype_id,QString googleSheetId);

    QNetworkAccessManager* m_network;
    SpreadsheetManager* m_spreadsheetAPI;
    QMap<QString,SpreadsheetQueue*> m_queues;
    QMap<QString,QString> m_skypeId2GoogleSheets;
    QString m_accessToken;

    QTimer m_procesTimer;
    int m_backofStepNumber;

    static const int m_processEveryMs=1000;
    static const int m_maxPowExpBackof=5;
    static const int m_widthRandom=1000;
    QMap<QString,SpreadsheetQueue*>::iterator m_iterator;
    //bool m_needCheckFolder;
    static const QString m_googleDriveFolder;
    static const QString m_googleAdditinalDriveFolder;
};

#endif // NETWORKMANAGER_H

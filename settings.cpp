#include "settings.h"
#include "common.h"
#include <QDateTime>

Settings::Settings(QObject *parent):QSettings(appDataPath() + APP_INNER_NAME + ".ini",QSettings::IniFormat,parent)
{

}

void Settings::addSkypeUser(QString skypeId, QString displayName, QString googleFileUid, bool isContact)
{
    bool needAdditionalPrepare=false;
    QString realSkypeIdval;

    if (skypeId.contains("/") || skypeId.contains("\\"))
    {
        int ind1 = skypeId.indexOf("/");
        int ind2 = skypeId.indexOf("\\");

        int ind = qMin(ind1,ind2);

        if (ind<0)
            ind = qMax(ind1,ind2);

        O_ASSERT(ind>0);

        realSkypeIdval = skypeId;
        needAdditionalPrepare=true;

        skypeId = skypeId.mid(0,ind);
    }

    QString key = "/logic/users/" + skypeId + "/";

    setValue(key+"name",displayName);
    setValue(key+"googleSheetId",googleFileUid);
    setValue(key+"contact",isContact);

    if(needAdditionalPrepare)
        setValue(key+"real",realSkypeIdval);
}

void Settings::setLastSync(QString skypeId, QDateTime date, QString lastInsertedregionA1note)
{
    QString key = "/logic/users/" + skypeId + "/";

    setValue(key+"lastSync",date);
    setValue(key+"lastRegion",lastInsertedregionA1note);
}

QStringList Settings::contactsSkypeIds()
{
//    qInfo() << childGroups();
//    qInfo() << childKeys();

    beginGroup("/logic/users/");

//    qInfo() << childGroups();
//    qInfo() << childKeys();

    QStringList res = childGroups();

    endGroup();

    QStringList::iterator it =  res.begin();

    while(it != res.end())
    {
        if (!value("/logic/users/" + (*it) +"/contact").toBool())
            it = res.erase(it);
        else
        {
            QString realSkype = value("/logic/users/" + (*it) +"/real").toString();

            if (!realSkype.isEmpty())
                (*it) = realSkype;
            it++;
        }
    }



    return res;
}

QStringList Settings::groupsSkypeIds()
{
        beginGroup("/logic/users/");

        QStringList res = childGroups();

        endGroup();

        QStringList::iterator it =  res.begin();

        while(it != res.end())
        {
            if (value("/logic/users/" + (*it) +"/contact").toBool())
                it = res.erase(it);
            else
            {
                QString realSkype = value("/logic/users/" + (*it) +"/real").toString();

                if (!realSkype.isEmpty())
                    (*it) = realSkype;
                it++;
            }
        }

        return res;
}

QString Settings::displayName(QString skypeId) const
{
    QString key = "/logic/users/" + skypeId + "/";

    return value(key + "name").toString();
}

QDateTime Settings::lastSync(QString skypeId) const
{
    QString key = "/logic/users/" + skypeId + "/";

    return value(key + "lastSync").toDateTime();
}

QMap<QString, QDateTime> Settings::lastSync()
{
    QMap<QString, QDateTime> res;

    beginGroup("/logic/users/");
    QStringList temp = childGroups();
    endGroup();

    for(QString skype_id:temp)
    {
        res[skype_id] = value("/logic/users/" + skype_id + "/lastSync").toDateTime();
    }

    return res;
}

QString Settings::lastInsertedregion(QString skypeId) const
{
    QString key = "/logic/users/" + skypeId + "/";

    return value(key + "lastRegion").toString();
}

void Settings::setGoogleSheetId(QString skypeId, QString googleSheetId)
{
   setValue("/logic/users/" + skypeId + "/googleSheetId",googleSheetId);
}

QString Settings::googleSheetId(QString skypeId) const
{
    return value("/logic/users/" + skypeId + "/googleSheetId").toString();
}

QMap<QString, QString> Settings::googleSheetIds()
{
    QMap<QString, QString> res;

    beginGroup("/logic/users/");
    QStringList temp = childGroups();
    endGroup();

    for(QString skype_id:temp)
    {
        res[skype_id] = value("/logic/users/" + skype_id + "/googleSheetId").toString();
    }

    return res;
}


int Settings::uploadInterval() const
{
    return value("/logic/network/upload_interval",m_uploadDefault).toInt();
}

void Settings::setLastSuccesfulRequestDate(const QDateTime &date)
{
    setValue("/gui/lastupdate",date);
}

QDateTime Settings::lastSuccesfulRequestDate() const
{
    return value("/gui/lastupdate").toDateTime();
}

void Settings::setInterestedMainDbs(QStringList pathes,QStringList names)
{
    setValue("/logic/interestedDbs",pathes);
    setValue("/logic/interestedDbsNames",names);
}

QStringList Settings::interestedMainDbs() const
{
    return value("/logic/interestedDbs").toStringList();
}

QStringList Settings::interestedMainDbsNames() const
{
    return value("/logic/interestedDbsNames").toStringList();
}

bool Settings::firstStart() const
{
  return value("/logic/firststart",true).toBool();
}

void Settings::setNonFirstStart()
{
    setValue("/logic/firststart",false);
}

void Settings::setGoogledrivefolder(QString name, QString id)
{
    setValue("/logic/folder/" + name,id);
}

QString Settings::googleDriveFolder(QString name) const
{
    return value("/logic/folder/" + name).toString();
}

void Settings::setRefreshToken(QString token)
{
    setValue("/logic/network/refreshtoken",token);
}

QString Settings::refreshtoken() const
{
    return value("/logic/network/refreshtoken").toString();
}

void Settings::setUploadInterval(int secs)
{
    setValue("/logic/network/upload_interval",secs);
}
